package com.eljacko.parkinghouse.datamodel.entity;

import com.eljacko.parkinghouse.datamodel.entity.base.BaseEntity;
import com.eljacko.parkinghouse.datamodel.entity.user.Vendor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.HashSet;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings({ "checkstyle:MethodName" })
public class BaseEntityUnitTest {

    @Test
    public final void equals_diffCode_Equal() {
        Vendor entity1 = new Vendor();
        entity1.setId(1L);

        BaseEntity entity2 = new Vendor();
        entity2.setId(entity1.getId());

        assertThat(entity1, is(equalTo(entity2)));
    }

    @Test
    public final void equals_diffId_NotEqual() {
        BaseEntity entity1 = new Vendor();
        entity1.setId(1L);

        BaseEntity entity2 = new Vendor();
        entity2.setId(2L);

        assertThat(entity1, not(equalTo(entity2)));
    }

    @Test
    public final void equals_diffIdNull_NotEqual() {
        BaseEntity entity1 = new Vendor();
        entity1.setId(1L);

        BaseEntity entity2 = new Vendor();

        assertThat(entity1, not(equalTo(entity2)));
    }

    @Test
    public final void equals_nullIds_NotEqual() {
        BaseEntity entity1 = new Vendor();

        BaseEntity entity2 = new Vendor();

        assertThat(entity1, not(equalTo(entity2)));
    }

    @Test
    public final void hashCode_sameId() {
        BaseEntity entity1 = new Vendor();
        entity1.setId(1L);

        BaseEntity entity2 = new Vendor();
        entity2.setId(entity1.getId());
        HashSet<BaseEntity> vendorSet = new HashSet<>(2);
        vendorSet.add(entity1);
        vendorSet.add(entity2);

        assertThat(vendorSet, hasSize(1));
    }

    @Test
    public final void hashCode_differentIds() {
        BaseEntity entity1 = new Vendor();
        entity1.setId(1L);

        BaseEntity entity2 = new Vendor();
        entity2.setId(2L);
        HashSet<BaseEntity> vendorSet = new HashSet<>(2);
        vendorSet.add(entity1);
        vendorSet.add(entity2);

        assertThat(vendorSet, hasSize(2));
        assertThat(vendorSet, hasItems(entity1, entity2));
    }

    @Test
    public final void hashCode_differentIdAndNull() {
        BaseEntity entity1 = new Vendor();

        BaseEntity entity2 = new Vendor();
        entity2.setId(2L);
        HashSet<BaseEntity> vendorSet = new HashSet<>(2);
        vendorSet.add(entity1);
        vendorSet.add(entity2);

        assertThat(vendorSet, hasSize(2));
        assertThat(vendorSet, hasItems(entity1, entity2));
    }

    @Test
    public final void hashCode_nullIds() {
        BaseEntity entity1 = new Vendor();
        BaseEntity entity2 = new Vendor();

        HashSet<BaseEntity> vendorSet = new HashSet<>(2);
        vendorSet.add(entity1);
        vendorSet.add(entity2);

        assertThat(vendorSet, hasSize(2));
        assertThat(vendorSet, hasItems(entity1, entity2));
    }

    @Test
    public final void hashCode_nullIds2() {
        BaseEntity entity1 = new Vendor();
        BaseEntity entity2 = new Vendor();

        HashMap<BaseEntity, BaseEntity> vendorSet = new HashMap<>(2);
        vendorSet.put(entity1, entity1);
        vendorSet.put(entity2, entity2);

        assertThat(vendorSet.size(), is(equalTo(2)));
    }
}
