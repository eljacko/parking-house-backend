package com.eljacko.parkinghouse.datamodel.entity.utils;

import com.eljacko.parkinghouse.datamodel.utils.DateUtils;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public final class UnitTestHelper {
    private UnitTestHelper() {
        throw new UnsupportedOperationException(
                "This is a utility class and cannot be instantiated");
    }

    public static Timestamp currentTimeAsTimestamp() {
        LocalDateTime today = LocalDateTime.now();
        return Timestamp.valueOf(today);
    }

    public static Timestamp currentTimeAsTimestampNoMillis() {
        LocalDateTime today = LocalDateTime.now().withNano(0);
        return Timestamp.valueOf(today);
    }

    public static Timestamp tommorowAsTimestampNoMillis() {
        LocalDateTime today = LocalDateTime.now().withNano(0);
        LocalDateTime tommorow = today.plusDays(1);
        return Timestamp.valueOf(tommorow);
    }

    public static Timestamp yesterdayAsTimestampNoMillis() {
        LocalDateTime today = LocalDateTime.now().withNano(0);
        LocalDateTime yesterday = today.minusDays(1);
        return Timestamp.valueOf(yesterday);
    }

    public static Date today() {
        LocalDate today = LocalDate.now();
        return DateUtils.asDate(today);
    }

    public static Date yesterday() {
        LocalDate yesterday = LocalDate.now().minusDays(1);
        return DateUtils.asDate(yesterday);
    }

    public static Date subtractedDay(final int daysToSubtract) {
        LocalDate day = LocalDate.now().minusDays(daysToSubtract);
        return DateUtils.asDate(day);
    }

}
