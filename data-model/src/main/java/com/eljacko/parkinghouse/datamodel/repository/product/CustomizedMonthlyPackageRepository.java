package com.eljacko.parkinghouse.datamodel.repository.product;

import com.eljacko.parkinghouse.datamodel.entity.product.MonthlyPackage;

public interface CustomizedMonthlyPackageRepository {

    MonthlyPackage getDefaultPackage(Long vendorId);

}
