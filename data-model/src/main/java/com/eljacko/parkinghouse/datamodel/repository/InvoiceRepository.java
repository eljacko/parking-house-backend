package com.eljacko.parkinghouse.datamodel.repository;

import com.eljacko.parkinghouse.datamodel.entity.invoice.Invoice;
import com.eljacko.parkinghouse.datamodel.entity.user.Customer;
import com.eljacko.parkinghouse.datamodel.entity.user.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

    Optional<Invoice> findById(Long id);

    Optional<Invoice> findByBillingMonthAndBillingYearAndCustomerAndVendor(
            String billingMonth, String billingYear, Customer customer, Vendor vendor);

}
