package com.eljacko.parkinghouse.datamodel.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class NameIdRec<T> {
    private T id;
    private String name;

    public NameIdRec(final T id, final String name) {
        super();
        this.id = id;
        this.name = name;
    }
}
