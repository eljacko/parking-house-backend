package com.eljacko.parkinghouse.datamodel.entity.product;

import com.eljacko.parkinghouse.datamodel.constant.ProductType;
import com.eljacko.parkinghouse.datamodel.constant.ProductTypeDiscriminator;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DynamicUpdate
@DynamicInsert
@Getter
@Setter
@ToString(callSuper = true)
@DiscriminatorValue(ProductTypeDiscriminator.MONTHLY_FEE)
public class MonthlyPackage extends Product {

    private static final long serialVersionUID = 1L;

    public MonthlyPackage() {
        super();
        this.setProductTypeId(ProductType.MONTHLY_FEE.getId());
    }
}
