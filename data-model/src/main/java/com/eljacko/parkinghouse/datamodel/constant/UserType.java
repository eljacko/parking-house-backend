package com.eljacko.parkinghouse.datamodel.constant;

import com.eljacko.parkinghouse.datamodel.constant.base.Type;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public enum UserType implements Type {
    VENDOR(1, "VENDOR"),
    CUSTOMER(2, "CUSTOMER");

    private final int id;
    private final String name;

    UserType(final Integer id, final String name) {
        this.id = id;
        this.name = name;
    }

    @JsonValue
    @Override
    public final int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    public boolean isUserType(final String userType) {
        return this.name.equals(userType);
    }

    public static UserType getEnum(final String value) {
        for (UserType pt : values()) {
            if (pt.getName().equalsIgnoreCase(value)) {
                return pt;
            }
        }
        throw new IllegalArgumentException("Unknown product type: " + value + ".");
    }


    public static boolean isCorrect(final String name) {
        return Arrays.stream(UserType.values()).anyMatch(e -> e.getName().equals(name));
    }

    public static Set<String> getAllUserTypes() {
        return Arrays.stream(UserType.values())
                .map(UserType::name).collect(Collectors.toSet());
    }

    public static final class UserTypeIdValues {
        public static final int VENDOR = 1;
        public static final int CUSTOMER = 2;

        private UserTypeIdValues() {
            throw new java.lang.UnsupportedOperationException(
                    "This is a constants class and cannot be instantiated");
        }
    }
}
