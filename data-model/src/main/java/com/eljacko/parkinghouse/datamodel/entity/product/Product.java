package com.eljacko.parkinghouse.datamodel.entity.product;

import com.eljacko.parkinghouse.datamodel.constant.FieldsLength;
import com.eljacko.parkinghouse.datamodel.constant.ValidationMessages;
import com.eljacko.parkinghouse.datamodel.entity.base.AuditableBaseEntity;
import com.eljacko.parkinghouse.datamodel.entity.user.Vendor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.util.CollectionUtils;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "products")
@ToString(callSuper = true)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type_id", discriminatorType = DiscriminatorType.INTEGER)
@DynamicUpdate
@DynamicInsert
@Setter
//@formatter:off
@GenericGenerator(
        name = "assigned-sequence",
        strategy = "com.eljacko.parkinghouse.datamodel.utils.AssignedSequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = "sequence_name",
                        value = "products_id_seq"),
                @org.hibernate.annotations.Parameter(
                        name = "optimizer",
                        value = "none")
        })
//@formatter:on
@SuppressWarnings({ "checkstyle:DesignForExtension", "checkstyle:MagicNumber" })
public abstract class Product extends AuditableBaseEntity {
    private static final long serialVersionUID = 7533033877550598198L;

    private String name;
    /**
     * Price in minor units of currency.
     */
    private Integer price;
    /**
     * Payment currency code (ISO 4217)
     */
    private Vendor vendor;
    private String currency;
    private ProductType type;

    // Hack to prevent Hibernate to fetch productInfo when using find method
    private Set<ProductInfo> productInfos;

    public Product() {
        super();
        setProductInfo(new ProductInfo());
    }

    @Size(max = FieldsLength.PRODUCT_NAME, message = ValidationMessages.MAX_LENGTH_EXCEEDED)
    @Column(length = FieldsLength.PRODUCT_NAME)
    public String getName() {
        return name;
    }

    @NotNull(message = ValidationMessages.NOT_NULL)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "vendor_id", nullable = false, updatable = false)
    public Vendor getVendor() {
        return vendor;
    }

    @NotNull(message = ValidationMessages.NOT_NULL)
    @Column(nullable = false)
    public Integer getPrice() {
        return price;
    }

    /**
     * Payment currency code (ISO 4217)
     */
    @NotNull(message = ValidationMessages.NOT_NULL)
    @Size(max = FieldsLength.CURRENCY_CODE, message = ValidationMessages.MAX_LENGTH_EXCEEDED)
    @Column(name = "currency", nullable = false, length = FieldsLength.CURRENCY_CODE)
    public String getCurrency() {
        return currency;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "type_id", insertable = false, updatable = false, nullable = false)
    public ProductType getType() {
        return type;
    }

    public void setType(final ProductType type) {
        this.type = type;
    }

    @Transient
    public void setProductTypeId(final int typeId) {
        this.setType(new ProductType());
        this.getType().setId(typeId);
    }

    @Transient
    public ProductInfo getProductInfo() {
        if (CollectionUtils.isEmpty(getProductInfos())) {
            return null;
        }
        return getProductInfos().iterator().next();
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "id")
    protected Set<ProductInfo> getProductInfos() {
        return productInfos;
    }

    protected void setProductInfos(final Set<ProductInfo> productInfos) {
        this.productInfos = productInfos;
    }

    @SuppressWarnings({ "checkstyle:FinalParameters" })
    @Transient
    public void setProductInfo(final ProductInfo productInfo) {
        setProductInfos(new HashSet<>(1));
        getProductInfos().add(productInfo);
        if (productInfo != null) {
            productInfo.setProduct(this);
        }
    }

}
