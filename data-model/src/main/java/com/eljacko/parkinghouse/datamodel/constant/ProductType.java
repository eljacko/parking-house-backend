package com.eljacko.parkinghouse.datamodel.constant;

import com.eljacko.parkinghouse.datamodel.constant.base.Type;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public enum ProductType implements Type {
    MONTHLY_FEE(1, "MONTHLY_FEE"),
    PARKING_FEE(2, "PARKING_FEE");

    private final int id;
    private final String name;

    ProductType(final Integer id, final String name) {
        this.id = id;
        this.name = name;
    }

    @JsonValue
    @Override
    public final int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    public boolean isProductType(final String productType) {
        return this.name.equals(productType);
    }

    public static ProductType getEnum(final String value) {
        for (ProductType pt : values()) {
            if (pt.getName().equalsIgnoreCase(value)) {
                return pt;
            }
        }
        throw new IllegalArgumentException("Unknown product type: " + value + ".");
    }


    public static boolean isCorrect(final String name) {
        return Arrays.stream(ProductType.values()).anyMatch(e -> e.getName().equals(name));
    }

    public static Set<String> getAllProductTypes() {
        return Arrays.stream(ProductType.values())
                .map(ProductType::name).collect(Collectors.toSet());
    }
}
