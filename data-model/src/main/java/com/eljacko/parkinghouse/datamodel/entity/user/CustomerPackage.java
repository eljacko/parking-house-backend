package com.eljacko.parkinghouse.datamodel.entity.user;

import com.eljacko.parkinghouse.datamodel.entity.base.BaseEntity;
import com.eljacko.parkinghouse.datamodel.entity.product.MonthlyPackage;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "customer_package")
@DynamicUpdate
@DynamicInsert
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
//@formatter:off
@GenericGenerator(
        name = "assigned-sequence",
        strategy = "com.eljacko.parkinghouse.datamodel.utils.AssignedSequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = "sequence_name",
                        value = "customer_package_id_seq"),
                @org.hibernate.annotations.Parameter(
                        name = "optimizer",
                        value = "none")
        })
//@formatter:on
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class CustomerPackage extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private Customer customer;
    private MonthlyPackage monthlyPackage;
    private Date startDate;
    private Date endDate;


    public CustomerPackage(final Customer customer, final MonthlyPackage monthlyPackage) {
        super();
        this.customer = customer;
        this.monthlyPackage = monthlyPackage;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "customer_id", nullable = false, updatable = false)
    public Customer getCustomer() {
        return customer;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "package_id", nullable = false, updatable = false)
    public MonthlyPackage getMonthlyPackage() {
        return monthlyPackage;
    }
}
