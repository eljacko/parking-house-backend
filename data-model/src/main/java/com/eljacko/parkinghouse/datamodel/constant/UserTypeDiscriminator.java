package com.eljacko.parkinghouse.datamodel.constant;

public final class UserTypeDiscriminator {
    public static final String VENDOR = "1";
    public static final String CUSTOMER = "2";

    private UserTypeDiscriminator() {
    }
}
