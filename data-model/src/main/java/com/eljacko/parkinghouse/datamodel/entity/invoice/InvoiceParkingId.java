package com.eljacko.parkinghouse.datamodel.entity.invoice;

import com.eljacko.parkinghouse.datamodel.constant.ValidationMessages;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Setter
@ToString
@NoArgsConstructor
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class InvoiceParkingId implements Serializable {
    private static final long serialVersionUID = -3374325285291298505L;

    private Long invoiceId;
    private Long parkingId;

    public InvoiceParkingId(final Long parkingId, final Long invoiceId) {
        super();
        this.parkingId = parkingId;
        this.invoiceId = invoiceId;
    }

    @NotNull(message = ValidationMessages.NOT_NULL)
    @Column(name = "parking_id", nullable = false, updatable = false)
    public Long getParkingId() {
        return parkingId;
    }

    @NotNull(message = ValidationMessages.NOT_NULL)
    @Column(name = "invoice_id", nullable = false, updatable = false)
    public Long getInvoiceId() {
        return invoiceId;
    }

    @Override
    public final int hashCode() {
        return Objects.hash(this.getInvoiceId(), this.getParkingId(), this.getClass());
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof InvoiceParkingId)) {
            return false;
        }
        InvoiceParkingId other = (InvoiceParkingId) obj;
        if (this.getInvoiceId() == null || other.getInvoiceId() == null
                || this.getParkingId() == null || other.getParkingId() == null) {
            return false;
        } else {
            return Objects.equals(this.getInvoiceId(), other.getInvoiceId())
                    && Objects.equals(this.getParkingId(), other.getParkingId());
        }
    }
}
