package com.eljacko.parkinghouse.datamodel.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class PackageDto {
    private Long id;
    private String name;
    private Integer packageFee;
    private Integer parkingFee;

    public PackageDto() {
        super();
    }
}
