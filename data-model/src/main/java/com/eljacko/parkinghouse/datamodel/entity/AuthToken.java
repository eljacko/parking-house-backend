package com.eljacko.parkinghouse.datamodel.entity;

import com.eljacko.parkinghouse.datamodel.constant.ValidationMessages;
import com.eljacko.parkinghouse.datamodel.entity.base.AuditableBaseEntity;
import com.eljacko.parkinghouse.datamodel.entity.user.User;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/*
 * Token for user authentication
 */
@Entity
@Table(name = "auth_token")
@Setter
@ToString(callSuper = true)
@DynamicUpdate
@DynamicInsert
//@formatter:off
@GenericGenerator(
        name = "assigned-sequence",
        strategy = "com.eljacko.parkinghouse.datamodel.utils.AssignedSequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = "sequence_name",
                        value = "auth_token_id_seq"),
                @org.hibernate.annotations.Parameter(
                        name = "optimizer",
                        value = "none")
                })
//@formatter:on
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class AuthToken extends AuditableBaseEntity {
    private static final long serialVersionUID = -7000756819949235723L;

    /*
     * A user to whom authentication token is made
     */
    private User user;

    /*
     * A code/key for user authentication
     */
    private String token;

    /*
     * When the token is set to expire
     */
    private Timestamp expiresAt;

    /**
     * A user to whom authentication token is made
     */
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", updatable = false)
    public User getUser() {
        return user;
    }

    /**
     * A code/key for user authentication
     */
    @NotNull(message = ValidationMessages.NOT_NULL)
    @Column(nullable = false)
    public String getToken() {
        return token;
    }

    /**
     * When the token is set to expire
     */
    @Column(name = "expires_at", nullable = true)
    public Timestamp getExpiresAt() {
        return expiresAt;
    }

}
