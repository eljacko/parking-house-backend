package com.eljacko.parkinghouse.datamodel.repository;

import com.eljacko.parkinghouse.datamodel.entity.user.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface VendorRepository extends JpaRepository<Vendor, Long> {

}
