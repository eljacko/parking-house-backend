package com.eljacko.parkinghouse.datamodel.constant.base;

public interface Type {
    int getId();
    String getName();
}
