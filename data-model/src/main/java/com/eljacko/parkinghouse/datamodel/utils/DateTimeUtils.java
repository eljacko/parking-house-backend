package com.eljacko.parkinghouse.datamodel.utils;


import com.eljacko.parkinghouse.datamodel.constant.DateFormats;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

public final class DateTimeUtils {

    private DateTimeUtils() {
    }

    public static Date parseRFC3339(final String timestampString) throws DateTimeParseException {
        return Date.from(DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(timestampString,
                Instant::from));
    }

    public static String formatRFC3339(final Date date) {
        final SimpleDateFormat format = new SimpleDateFormat(DateFormats.RFC3339);
        return format.format(date);
    }

    public static OffsetDateTime parseRFC3339ToDateTime(final String timestampString) {
        return DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(timestampString,
                OffsetDateTime::from);
    }

    public static String formatRFC3339(final OffsetDateTime time) {
        return DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(time);
    }
}
