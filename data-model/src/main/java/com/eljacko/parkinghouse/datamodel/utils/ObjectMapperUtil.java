package com.eljacko.parkinghouse.datamodel.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public final class ObjectMapperUtil {
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private ObjectMapperUtil() {
        throw new UnsupportedOperationException(
                "This is a utility class and cannot be instantiated");
    }

    public static <T> T fromString(final String string, final Class<T> clazz) {
        try {
            return OBJECT_MAPPER.readValue(string.getBytes("UTF-8"), clazz);
        } catch (IOException e) {
            throw (IllegalArgumentException) new IllegalArgumentException(
                    "The given string value: " + string + " cannot be transformed to Json object")
                    .initCause(e);
        }
    }

    public static String toString(final Object value) {
        try {
            return OBJECT_MAPPER.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw (IllegalArgumentException) new IllegalArgumentException(
                    "The given Json object value: " + value + " cannot be transformed to a String")
                    .initCause(e);
        }
    }


    @SuppressWarnings("unchecked")
    public static <T> T clone(final T value) {
        return fromString(toString(value), (Class<T>) value.getClass());
    }
}
