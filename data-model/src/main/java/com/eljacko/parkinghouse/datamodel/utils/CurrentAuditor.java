package com.eljacko.parkinghouse.datamodel.utils;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
public class CurrentAuditor implements Serializable {
    private static final long serialVersionUID = 2482953193467674453L;
    private String table;
    private Long id;
    private String name;

    public CurrentAuditor(final String table) {
        super();
        this.table = table;
    }

    public CurrentAuditor(final String table, final Long id, final String name) {
        super();
        this.table = table;
        this.id = id;
        this.name = name;
    }

    public final String toJsonString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append("\"table\":\"");
        builder.append(table);
        builder.append("\",");
        builder.append("\"id\":");
        builder.append(id);
        builder.append(",\"name\":\"");
        builder.append(name);
        builder.append("\"");
        builder.append("}");
        return builder.toString();
    }

    @Override
    public final String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(table);
        builder.append(":");
        builder.append(id);
        builder.append(":");
        builder.append(name);
        return builder.toString();
    }
}
