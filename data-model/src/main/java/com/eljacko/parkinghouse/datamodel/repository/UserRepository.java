package com.eljacko.parkinghouse.datamodel.repository;

import com.eljacko.parkinghouse.datamodel.dto.UserLoginDto;
import com.eljacko.parkinghouse.datamodel.entity.user.User;
import com.eljacko.parkinghouse.datamodel.repository.user.CustomizedUserRepository;
import com.eljacko.parkinghouse.datamodel.repository.user.UserCommonRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends UserCommonRepository<User>, CustomizedUserRepository {

    // @formatter:off
    @Query("SELECT new com.eljacko.parkinghouse.datamodel.dto.UserLoginDto( "
            + " u.id, u.email, u.password) "
            + " FROM User u "
            + " WHERE u.email = :login")
    // @formatter:on
    List<UserLoginDto> getUserLogin(@Param("login") String login);

}
