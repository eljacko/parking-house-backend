package com.eljacko.parkinghouse.datamodel.entity.user;

import com.eljacko.parkinghouse.datamodel.constant.UserType;
import com.eljacko.parkinghouse.datamodel.constant.UserTypeDiscriminator;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DynamicUpdate
@DynamicInsert
@Getter
@Setter
@ToString(callSuper = true)
@DiscriminatorValue(UserTypeDiscriminator.CUSTOMER)
public class Customer extends User {

    private static final long serialVersionUID = 1L;

    public Customer() {
        super();
        this.setUserTypeId(UserType.CUSTOMER.getId());
    }
}
