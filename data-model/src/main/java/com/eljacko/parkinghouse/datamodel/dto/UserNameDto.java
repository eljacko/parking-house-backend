package com.eljacko.parkinghouse.datamodel.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class UserNameDto {
    private Long id;
    private String name;
    private Integer typeId;

    public UserNameDto() {
    }

    public UserNameDto(final Long userId, final String name,
            final Integer typeId) {
        super();
        this.id = userId;
        this.name = name;
        this.typeId = typeId;
    }

}
