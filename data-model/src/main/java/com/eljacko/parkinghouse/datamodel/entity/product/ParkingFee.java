package com.eljacko.parkinghouse.datamodel.entity.product;

import com.eljacko.parkinghouse.datamodel.constant.ProductType;
import com.eljacko.parkinghouse.datamodel.constant.ProductTypeDiscriminator;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DynamicUpdate
@DynamicInsert
@Getter
@Setter
@ToString(callSuper = true)
@DiscriminatorValue(ProductTypeDiscriminator.PARKING_FEE)
public class ParkingFee extends Product {

    private static final long serialVersionUID = 1L;

    public ParkingFee() {
        super();
        this.setProductTypeId(ProductType.PARKING_FEE.getId());
    }
}
