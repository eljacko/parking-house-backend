package com.eljacko.parkinghouse.datamodel.repository.product;

import com.eljacko.parkinghouse.datamodel.entity.product.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductInfoRepository extends JpaRepository<ProductInfo, Long> {


}
