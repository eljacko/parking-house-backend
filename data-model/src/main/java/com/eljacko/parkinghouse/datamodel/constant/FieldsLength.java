package com.eljacko.parkinghouse.datamodel.constant;

public final class FieldsLength {

    public static final int CLAIM_TYPE_CODE = 5;
    public static final int CURRENCY_CODE = 3;
    public static final int PAYMENT_STATUS = 5;
    public static final int PRODUCT_NAME = 64;
    public static final int EMAIL = 64;
    public static final int PRODUCT_TYPE_NAME = 64;
    public static final int USER_TYPE_NAME = 64;

    private FieldsLength() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
