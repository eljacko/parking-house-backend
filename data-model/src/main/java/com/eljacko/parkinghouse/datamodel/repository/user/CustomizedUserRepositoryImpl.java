package com.eljacko.parkinghouse.datamodel.repository.user;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Getter
@Setter
@Slf4j
public class CustomizedUserRepositoryImpl implements CustomizedUserRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public final boolean existsOtherWithSameName(final String name, final Long excludedId) {
        Assert.notNull(name, "Name must not be null");
        String additionalWherePart = "";
        if (excludedId != null) {
            additionalWherePart = " AND u.id <> :id ";
        }
        // @formatter:off
        String hql = "SELECT EXISTS ( "
                + "SELECT true "
                + " FROM public.users u "
                + " WHERE u.name = :name "
                + additionalWherePart
                + ") AS retVal";
        // @formatter:on
        Query q = getEm().createNativeQuery(hql);
        q.setParameter("name", name);
        if (excludedId != null) {
            q.setParameter("id", excludedId);
        }
        return (Boolean) q.getSingleResult();
    }
}
