package com.eljacko.parkinghouse.datamodel.entity.product;

import com.eljacko.parkinghouse.datamodel.constant.FieldsLength;
import com.eljacko.parkinghouse.datamodel.constant.ValidationMessages;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * The persistent class for the product_type database table.
 *
 */
@Entity
@Table(name = "product_type")
@Setter
@ToString
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class ProductType implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String name;

    public ProductType() {
    }

    @Id
    @Column(unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    @Size(max = FieldsLength.PRODUCT_TYPE_NAME, message = ValidationMessages.MAX_LENGTH_EXCEEDED)
    @Column(unique = true, length = FieldsLength.PRODUCT_TYPE_NAME)
    public String getName() {
        return name;
    }

    @SuppressWarnings({ "checkstyle:AvoidInlineConditionals" })
    @Override
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.getId() == null) ? 0 : this.getId().hashCode());
        result = prime * result + getClass().hashCode();
        return result;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ProductType)) {
            return false;
        }
        ProductType other = (ProductType) obj;
        if (this.getId() == null || other.getId() == null) {
            return false;
        } else if (!this.getId().equals(other.getId())) {
            return false;
        }
        return true;
    }

}
