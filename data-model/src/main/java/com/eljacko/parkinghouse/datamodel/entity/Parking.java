package com.eljacko.parkinghouse.datamodel.entity;

import com.eljacko.parkinghouse.datamodel.entity.base.AuditableBaseEntity;
import com.eljacko.parkinghouse.datamodel.entity.user.Customer;
import com.eljacko.parkinghouse.datamodel.entity.user.Vendor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "parking")
@ToString(callSuper = true)
@Setter
@SuppressWarnings({ "checkstyle:DesignForExtension" })
//@formatter:off
@GenericGenerator(
        name = "assigned-sequence",
        strategy = "com.eljacko.parkinghouse.datamodel.utils.AssignedSequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = "sequence_name",
                        value = "parking_id_seq"),
                @org.hibernate.annotations.Parameter(
                        name = "optimizer",
                        value = "none")
        })
//@formatter:on
public class Parking extends AuditableBaseEntity {
    private static final long serialVersionUID = 3468395790102398417L;

    /* Customer related to the parking */
    private Customer customer;
    /* Vendor related to the parking location */
    private Vendor vendor;
    /** Record parking start time */
    private Date startTime;
    /** Record parking end time */
    private Date endTime;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "customer_id", nullable = false)
    public Customer getCustomer() {
        return customer;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "vendor_id", nullable = false)
    public Vendor getVendor() {
        return vendor;
    }

    /** Record parking start time timestamp */
    @Column(name = "start_time", nullable = false)
    public Date getStartTime() {
        return startTime;
    }

    /** Record parking end time timestamp */
    @Column(name = "end_time")
    public Date getEndTime() {
        return endTime;
    }

}
