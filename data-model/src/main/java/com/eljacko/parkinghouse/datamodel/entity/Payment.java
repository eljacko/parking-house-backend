package com.eljacko.parkinghouse.datamodel.entity;

import com.eljacko.parkinghouse.datamodel.constant.FieldsLength;
import com.eljacko.parkinghouse.datamodel.constant.PaymentStatus;
import com.eljacko.parkinghouse.datamodel.constant.ValidationMessages;
import com.eljacko.parkinghouse.datamodel.entity.base.AuditableBaseEntity;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

/**
 * Payment records
 */
@Entity
@Table(name = "payment")
@Setter
@ToString(callSuper = true)
@DynamicUpdate
@DynamicInsert
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class Payment extends AuditableBaseEntity {

    private static final long serialVersionUID = 346142990102398417L;

    private Timestamp createdAt;

    private Long invoiceId;
    /**
     * Payment amount in minor units of currency. Incoming amounts with positive sign, outgoing with
     * negative sign.
     */
    private Integer amount;

    /**
     * Payment currency code (ISO 4217)
     */
    private String currency;
    /**
     * Payment type (PAY -- ordinary payment, REV -- payment reversal)
     */
    private String type;
    /**
     * Payment status (OK -- success, FAIL -- failure)
     */
    private String status;

    @Column(name = "created_at", nullable = false)
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    @NotNull(message = ValidationMessages.NOT_NULL)
    @Column(name = "invoice_id", nullable = false, updatable = false)
    public Long getInvoiceId() {
        return invoiceId;
    }

    /**
     * Payment amount in minor units of currency. Incoming amounts with positive sign, outgoing with
     * negative sign.
     */
    @NotNull(message = ValidationMessages.NOT_NULL)
    @Column(nullable = false)
    public Integer getAmount() {
        return amount;
    }

    /**
     * Payment currency code (ISO 4217)
     */
    @NotNull(message = ValidationMessages.NOT_NULL)
    @Size(max = FieldsLength.CURRENCY_CODE, message = ValidationMessages.MAX_LENGTH_EXCEEDED)
    @Column(name = "currency", nullable = false, length = FieldsLength.CURRENCY_CODE)
    public String getCurrency() {
        return currency;
    }

    /**
     * Payment type (PAY -- ordinary payment, REV -- payment reversal)
     */
    @NotNull(message = ValidationMessages.NOT_NULL)
    @Size(max = FieldsLength.CLAIM_TYPE_CODE, message = ValidationMessages.MAX_LENGTH_EXCEEDED)
    @Column(name = "type", nullable = false, length = FieldsLength.CLAIM_TYPE_CODE)
    public String getType() {
        return type;
    }

    /**
     * Payment status (OK -- success, FAIL -- failure)
     */
    @NotNull(message = ValidationMessages.NOT_NULL)
    @Size(max = FieldsLength.PAYMENT_STATUS, message = ValidationMessages.MAX_LENGTH_EXCEEDED)
    @Column(name = "status", nullable = false, length = FieldsLength.PAYMENT_STATUS)
    public String getStatus() {
        return status;
    }

    protected void setStatus(final String status) {
        this.status = status;
    }

    @Transient
    public void setPaymentStatus(final PaymentStatus paymentStatus) {
        if (paymentStatus != null) {
            setStatus(paymentStatus.getStatus());
        }
    }

    @Transient
    public PaymentStatus getPaymentStatus() {
        if (status == null) {
            return null;
        }
        return PaymentStatus.getEnum(status);
    }

}
