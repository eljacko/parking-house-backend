package com.eljacko.parkinghouse.datamodel.repository.invoice;

import com.eljacko.parkinghouse.datamodel.entity.invoice.InvoiceParking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceParkingRepository extends JpaRepository<InvoiceParking, Long> {

}
