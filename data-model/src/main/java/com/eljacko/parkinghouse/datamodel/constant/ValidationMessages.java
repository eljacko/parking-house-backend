package com.eljacko.parkinghouse.datamodel.constant;

public final class ValidationMessages {
    // @formatter:off
    public static final String MAX_LENGTH_EXCEEDED =
            "Value is longer than {max} characters";
    public static final String NOT_NULL =
            "Value must not be null";

    // @formatter:on
    private ValidationMessages() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }

}
