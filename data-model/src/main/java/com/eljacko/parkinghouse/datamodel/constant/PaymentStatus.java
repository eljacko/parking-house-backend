package com.eljacko.parkinghouse.datamodel.constant;

import lombok.Getter;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public enum PaymentStatus {

    //@formatter:off
        /**
         * Success (OK)
         */
        OK("OK"),
        /**
         * Failure (FAIL)
         */
        FAIL("FAIL");
      //@formatter:on

    @Getter
    private final String status;

    PaymentStatus(final String status) {
        this.status = status;
    }

    public boolean equalsId(final String passedStatus) {
        if (passedStatus == null) {
            return false;
        }
        return passedStatus.equals(getStatus());
    }

    public static Set<String> getPaymentStatuses() {
        return Arrays.stream(PaymentStatus.values()).map(PaymentStatus::getStatus)
                .collect(Collectors.toSet());
    }

    public static PaymentStatus getEnum(final String value) {
        if (value != null) {
            for (PaymentStatus v : values()) {
                if (value.equals(v.getStatus())) {
                    return v;
                }
            }
        }
        throw new IllegalArgumentException("Unknown status:" + value + ". Allowed statuses are:"
                + PaymentStatus.getPaymentStatuses().toString());
    }

    @Override
    public String toString() {
        return String.valueOf(getStatus());
    }

    public static final class PaymentStatusValues {
        public static final String OK = "OK";
        public static final String FAIL = "FAIL";

        private PaymentStatusValues() {
            throw new UnsupportedOperationException(
                    "This is a constants class and cannot be instantiated");
        }
    }
}
