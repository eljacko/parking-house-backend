package com.eljacko.parkinghouse.datamodel.dto;

import com.eljacko.parkinghouse.datamodel.entity.Parking;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;


@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ParkingDto {
    private Long id;
    private Date startTime;
    private Date endTime;

    public ParkingDto(final Parking parking) {
        super();
        this.id = parking.getId();
        this.startTime = parking.getStartTime();
        this.endTime = parking.getEndTime();
    }

}
