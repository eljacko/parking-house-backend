package com.eljacko.parkinghouse.datamodel.repository.product;

import com.eljacko.parkinghouse.datamodel.entity.product.MonthlyPackage;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Getter
@Setter
@Slf4j
public class CustomizedMonthlyPackageRepositoryImpl implements CustomizedMonthlyPackageRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public MonthlyPackage getDefaultPackage(Long vendorId) {
        Assert.notNull(vendorId, "Vendor id must not be null");
        TypedQuery<MonthlyPackage> query
                = em.createQuery(
                "SELECT mp FROM MonthlyPackage mp JOIN mp.productInfos i "
                        + "WHERE mp.id = :vendorId AND i.defaultPackage",
                MonthlyPackage.class);
        query.setParameter("vendorId", vendorId);
        return query.getSingleResult();
    }

}
