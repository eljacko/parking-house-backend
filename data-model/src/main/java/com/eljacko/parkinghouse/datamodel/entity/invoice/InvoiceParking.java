package com.eljacko.parkinghouse.datamodel.entity.invoice;

import com.eljacko.parkinghouse.datamodel.entity.Parking;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "invoice_parking")
@DynamicUpdate
@DynamicInsert
@Setter
@ToString(callSuper = true)
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class InvoiceParking implements Serializable {
    private static final long serialVersionUID = 1L;

    private InvoiceParkingId invoiceParkingId;
    private Parking parking;
    private Invoice invoice;
    private Integer parkingFee;

    protected InvoiceParking() {
        super();
    }

    public InvoiceParking(final Parking parking, final Invoice invoice) {
        super();
        this.parking = parking;
        this.invoice = invoice;
        this.invoiceParkingId = new InvoiceParkingId(parking.getId(), invoice.getId());
    }

    @EmbeddedId
    public InvoiceParkingId getInvoiceParkingId() {
        return invoiceParkingId;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @MapsId("parkingId")
    public Parking getParking() {
        return parking;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @MapsId("invoiceId")
    public Invoice getInvoice() {
        return invoice;
    }

    public Integer getParkingFee() {
        return parkingFee;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof InvoiceParking)) {
            return false;
        }
        InvoiceParking other = (InvoiceParking) obj;
        return Objects.equals(this.getInvoiceParkingId(), other.getInvoiceParkingId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(invoiceParkingId);
    }
}
