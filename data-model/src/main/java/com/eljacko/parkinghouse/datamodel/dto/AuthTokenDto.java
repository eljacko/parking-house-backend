package com.eljacko.parkinghouse.datamodel.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class AuthTokenDto {
    private Long id;
    private Long userId;
    private String token;
    private Timestamp expiresAt;

    public AuthTokenDto() {
    }

    public AuthTokenDto(final Long id, final Long userId, final String token,
                        final java.util.Date expiresAt) {
        super();
        this.id = id;
        this.userId = userId;
        this.token = token;
        if (expiresAt != null) {
            this.expiresAt = new Timestamp(expiresAt.getTime());
        }
    }

}
