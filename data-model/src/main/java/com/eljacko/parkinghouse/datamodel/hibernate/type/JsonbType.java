package com.eljacko.parkinghouse.datamodel.hibernate.type;

import com.eljacko.parkinghouse.datamodel.utils.ObjectMapperUtil;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Objects;
import java.util.Properties;

/**
 * {@code JsonbType} converts a Postgres JSONB data type to a Java object and
 * vice versa
 * <p/>
 *
 */
public class JsonbType implements UserType, ParameterizedType {
    public static final String CLASS = "className";
    @SuppressWarnings("PMD.BeanMembersShouldSerialize")
    private Class<?> clazz;

    /**
     * The class returned by <tt>nullSafeGet()</tt>
     *
     * @return Class
     */
    @Override
    public final Class<?> returnedClass() {
        return clazz;
    }

    /**
     * Return the SQL type codes for the columns mapped by this type.
     */
    @Override
    public final int[] sqlTypes() {
        return new int[] {Types.JAVA_OBJECT };
    }

    /**
     * Pass the configured type parameters to the implementation. Method sets
     * the class of mapped object.
     *
     * @param parameters
     *            includes all the configuration settings available with this
     *            instance of custom mapping. The parameters are set in the
     *            mapping document.
     */
    @Override
    public final void setParameterValues(final Properties parameters) {
        String className = parameters.getProperty(CLASS);

        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException cnfe) {
            throw new HibernateException("Class not found", cnfe);
        }
    }

    @Override
    public final Object assemble(final Serializable cached, final Object owner)
            throws HibernateException {
        return deepCopy(cached);
    }

    @Override
    public final Object deepCopy(final Object value) throws HibernateException {
        if (value != null) {
            return ObjectMapperUtil.clone(value);
        }
        return null;
    }

    @Override
    public final Serializable disassemble(final Object value) throws HibernateException {
        return ObjectMapperUtil.toString(value);
    }

    @Override
    public final boolean equals(final Object x, final Object y) throws HibernateException {
        return Objects.equals(x, y);
    }

    @Override
    public final int hashCode(final Object x) throws HibernateException {
        return Objects.hashCode(x);
    }

    /**
     *  Determines whether mapped classes are mutable or not
     */
    @Override
    public final boolean isMutable() {
        return true;
    }

    @Override
    public final Object nullSafeGet(final ResultSet rs, final String[] names,
            final SharedSessionContractImplementor session, final Object owner)
            throws HibernateException, SQLException {
        String json = rs.getString(names[0]);

        if (json == null) {
            return null;
        }
        return ObjectMapperUtil.fromString(json, returnedClass());
    }

    @Override
    public final void nullSafeSet(final PreparedStatement st, final Object value, final int index,
            final SharedSessionContractImplementor session)
            throws HibernateException, SQLException {
        if (value == null) {
            st.setNull(index, Types.OTHER);
        } else {
            st.setObject(index, ObjectMapperUtil.toString(value), Types.OTHER);
        }

    }

    @Override
    public final Object replace(final Object original, final Object target, final Object owner)
            throws HibernateException {
        return deepCopy(original);
    }

}
