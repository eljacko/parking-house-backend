package com.eljacko.parkinghouse.datamodel.utils;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public final class DateUtils {
    private DateUtils() {
        throw new UnsupportedOperationException(
                "This is a utility class and cannot be instantiated");
    }

    public static Date asDate(final LocalDate localDate) {
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static Date addMonth(final Date fromDate) {
        return asDate(fromDate.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate()
                .plusMonths(1));
    }
}
