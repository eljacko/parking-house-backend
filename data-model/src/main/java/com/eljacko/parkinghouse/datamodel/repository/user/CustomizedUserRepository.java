package com.eljacko.parkinghouse.datamodel.repository.user;

public interface CustomizedUserRepository {

    boolean existsOtherWithSameName(final String name, final Long excludedId);

}
