package com.eljacko.parkinghouse.datamodel.entity.base;


import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

@MappedSuperclass
@Setter
@ToString(callSuper = true)
@SuppressWarnings({ "checkstyle:DesignForExtension" })
@EntityListeners(AuditingEntityListener.class)
public abstract class AuditableEntity implements java.io.Serializable {
    private static final long serialVersionUID = -5442197953338527484L;
    private Date created;
    private String createdBy;
    private Date modified;
    private String modifiedBy;

    @Column(nullable = false, updatable = false)
    @CreatedDate
    @Temporal(TIMESTAMP)
    public Date getCreated() {
        return created;
    }

    @Column(name = "created_by", updatable = false)
    @CreatedBy
    public String getCreatedBy() {
        return createdBy;
    }

    @LastModifiedDate
    @Temporal(TIMESTAMP)
    public Date getModified() {
        return modified;
    }

    @Column(name = "modified_by")
    @LastModifiedBy
    public String getModifiedBy() {
        return modifiedBy;
    }

}
