package com.eljacko.parkinghouse.datamodel.constant;

public final class ProductTypeDiscriminator {
    public static final String MONTHLY_FEE = "1";
    public static final String PARKING_FEE = "2";

    private ProductTypeDiscriminator() {
    }
}
