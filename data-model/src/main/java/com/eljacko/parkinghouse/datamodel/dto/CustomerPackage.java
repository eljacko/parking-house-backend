package com.eljacko.parkinghouse.datamodel.dto;

import com.eljacko.parkinghouse.datamodel.entity.invoice.Invoice;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class CustomerPackage {
    private Long id;
    private Date createdAt;
    private Integer totalAmount;
    private String currency;
    private String billingMonth;
    private String billingYear;
    private List<ParkingDto> parkings;
    private Long paymentId;

    public CustomerPackage(final Invoice invoice) {
        super();
        this.currency = "EUR";
        this.id = invoice.getId();
        this.createdAt = invoice.getCreated();
        this.totalAmount = invoice.getTotalAmount();
        this.paymentId = invoice.getPayment().getId();
        this.billingMonth = invoice.getBillingMonth();
        this.billingYear = invoice.getBillingYear();
        this.parkings = invoice.getParkings().stream().map(ip -> new ParkingDto(ip.getParking()))
                .collect(Collectors.toList());
    }

}
