package com.eljacko.parkinghouse.datamodel.repository.product;

import com.eljacko.parkinghouse.datamodel.entity.product.ParkingFee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingFeeRepository extends JpaRepository<ParkingFee, Long> {
}
