package com.eljacko.parkinghouse.datamodel.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class PaymentDto {
    private Long id;
    private Long invoiceId;
    private Timestamp createdAt;
    private Integer amount;
    private String currency;
    private String type;
    private String status;

    @SuppressWarnings("checkstyle:ParameterNumber")
    public PaymentDto(final Long id, final Long invoiceId, final java.util.Date createdAt,
              final Integer amount, final String currency, final String type, final String status) {
        super();
        this.id = id;
        this.invoiceId = invoiceId;
        this.createdAt = new Timestamp(createdAt.getTime());
        this.amount = amount;
        this.currency = currency;
        this.type = type;
        this.status = status;
    }

    public PaymentDto() {
        super();
    }

}
