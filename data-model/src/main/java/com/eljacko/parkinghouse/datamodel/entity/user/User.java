package com.eljacko.parkinghouse.datamodel.entity.user;

import com.eljacko.parkinghouse.datamodel.constant.FieldsLength;
import com.eljacko.parkinghouse.datamodel.constant.ValidationMessages;
import com.eljacko.parkinghouse.datamodel.entity.base.AuditableBaseEntity;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

@Entity
@Table(name = "users")
@ToString(callSuper = true)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type_id", discriminatorType = DiscriminatorType.INTEGER)
@DynamicUpdate
@DynamicInsert
@Setter
@SuppressWarnings({ "checkstyle:DesignForExtension" })
//@formatter:off
@GenericGenerator(
        name = "assigned-sequence",
        strategy = "com.eljacko.parkinghouse.datamodel.utils.AssignedSequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = "sequence_name",
                        value = "users_id_seq"),
                @org.hibernate.annotations.Parameter(
                        name = "optimizer",
                        value = "none")
        })
//@formatter:on
public abstract class User extends AuditableBaseEntity {
    private static final long serialVersionUID = 8098043870298083510L;

    private String name;
    private CustomerPackage customerPackage;
    private UserType type;
    private String email;
    private String password;
    private Timestamp passwordCreated;

    @NotNull(message = ValidationMessages.NOT_NULL)
    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "type_id", insertable = false, updatable = false, nullable = false)
    public UserType getType() {
        return type;
    }

    public void setType(final UserType type) {
        this.type = type;
    }

    @Transient
    public void setUserTypeId(final int typeId) {
        this.setType(new UserType());
        this.getType().setId(typeId);
    }

    public String getPassword() {
        return password;
    }

    @Size(max = FieldsLength.EMAIL, message = ValidationMessages.MAX_LENGTH_EXCEEDED)
    @Column(length = FieldsLength.EMAIL)
    public String getEmail() {
        return email;
    }

    @Column(name = "password_created")
    public Timestamp getPasswordCreated() {
        return passwordCreated;
    }

}
