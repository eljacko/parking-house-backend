package com.eljacko.parkinghouse.datamodel.repository.invoice;

import com.eljacko.parkinghouse.datamodel.dto.PaymentDto;
import com.eljacko.parkinghouse.datamodel.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {
    // @formatter:off
    @Query("SELECT new com.eljacko.parkinghouse.datamodel.dto.PaymentDto("
            + " p.id, p.invoiceId, p.createdAt, p.amount, p.currency, p.type, p.status) "
            + " FROM Payment p "
            + " WHERE p.invoiceId = :invoiceId")
    // @formatter:on
    List<PaymentDto> getByInvoiceId(@Param("invoiceId") Long invoiceId);

}
