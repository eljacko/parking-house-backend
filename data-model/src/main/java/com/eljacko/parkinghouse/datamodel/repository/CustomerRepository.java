package com.eljacko.parkinghouse.datamodel.repository;

import com.eljacko.parkinghouse.datamodel.entity.user.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
