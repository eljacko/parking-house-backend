package com.eljacko.parkinghouse.datamodel.repository.user;

import com.eljacko.parkinghouse.datamodel.entity.user.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserTypeRepository extends JpaRepository<UserType, Integer> {

}
