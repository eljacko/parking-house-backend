package com.eljacko.parkinghouse.datamodel.entity.product;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.proxy.HibernateProxyHelper;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "product_info")
@DynamicUpdate
@DynamicInsert
@Setter
@NoArgsConstructor
@ToString(callSuper = true, exclude = "product")
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class ProductInfo implements Serializable {
    private static final long serialVersionUID = 6238942447448244222L;

    private Long id;
    private Product product;
    private String memo;
    private ParkingFee parkingFee;
    private Integer maximumInvoice;
    private boolean defaultPackage;

    public String getMemo() {
        return memo;
    }

    @Id
    @GeneratedValue(generator = "foreigngen")
    @GenericGenerator(name = "foreigngen", strategy = "foreign",
            parameters = @Parameter(name = "property", value = "product"))
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @PrimaryKeyJoinColumn(name = "id")
    public Product getProduct() {
        return product;
    }

    // todo check join
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @PrimaryKeyJoinColumn(name = "id")
    public ParkingFee getParkingFee() {
        return parkingFee;
    }

    public Integer getMaximumInvoice() {
        return maximumInvoice;
    }

    public boolean isDefaultPackage() {
        return defaultPackage;
    }

    @SuppressWarnings({ "checkstyle:AvoidInlineConditionals" })
    @Override
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.getId() == null) ? 0 : this.getId().hashCode());
        result = prime * result + getClass().hashCode();
        return result;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ProductInfo)) {
            return false;
        }
        // workaround for Hibernate proxies
        if (!HibernateProxyHelper.getClassWithoutInitializingProxy(this)
                .equals(HibernateProxyHelper.getClassWithoutInitializingProxy(obj))) {
            return false;
        }
        ProductInfo other = (ProductInfo) obj;
        if (this.getProduct().getId() == null || other.getProduct().getId() == null) {
            return false;
        } else if (!this.getProduct().getId().equals(other.getProduct().getId())) {
            return false;
        }
        return true;
    }

}
