package com.eljacko.parkinghouse.datamodel.entity.invoice;

import com.eljacko.parkinghouse.datamodel.constant.ValidationMessages;
import com.eljacko.parkinghouse.datamodel.entity.Payment;
import com.eljacko.parkinghouse.datamodel.entity.base.AuditableBaseEntity;
import com.eljacko.parkinghouse.datamodel.entity.product.MonthlyPackage;
import com.eljacko.parkinghouse.datamodel.entity.user.Customer;
import com.eljacko.parkinghouse.datamodel.entity.user.Vendor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "invoices")
@ToString(callSuper = true)
@Setter
@SuppressWarnings({ "checkstyle:DesignForExtension" })
//@formatter:off
@GenericGenerator(
        name = "assigned-sequence",
        strategy = "com.eljacko.parkinghouse.datamodel.utils.AssignedSequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = "sequence_name",
                        value = "invoices_id_seq"),
                @org.hibernate.annotations.Parameter(
                        name = "optimizer",
                        value = "none")
        })
//@formatter:on
public class Invoice extends AuditableBaseEntity {
    private static final long serialVersionUID = 3468395790102398417L;

    private Customer customer;
    private Vendor vendor;
    private Date dateIssued;
    private MonthlyPackage monthlyPackage;
    private Set<InvoiceParking> parkings;
    private Payment payment;
    private String billingMonth;
    private String billingYear;
    private Integer totalAmount;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "customer_id", nullable = false)
    public Customer getCustomer() {
        return customer;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "vendor_id", nullable = false)
    public Vendor getVendor() {
        return vendor;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "payment_id", nullable = false)
    public Payment getPayment() {
        return payment;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "package_id", nullable = false)
    public MonthlyPackage getMonthlyPackage() {
        return monthlyPackage;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "invoice")
    public Set<InvoiceParking> getParkings() {
        return parkings;
    }

    @NotNull(message = ValidationMessages.NOT_NULL)
    @Column(name = "date_issued", nullable = false)
    public Date getDateIssued() {
        return dateIssued;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public String getBillingMonth() {
        return billingMonth;
    }

    public String getBillingYear() {
        return billingYear;
    }

}
