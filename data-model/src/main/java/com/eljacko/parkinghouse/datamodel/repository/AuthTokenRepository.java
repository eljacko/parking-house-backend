package com.eljacko.parkinghouse.datamodel.repository;

import com.eljacko.parkinghouse.datamodel.dto.AuthTokenDto;
import com.eljacko.parkinghouse.datamodel.entity.AuthToken;
import com.eljacko.parkinghouse.datamodel.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthTokenRepository extends JpaRepository<AuthToken, Long> {
    // @formatter:off
    @Query("SELECT new com.eljacko.parkinghouse.datamodel.dto.AuthTokenDto("
            + " a.id, a.user.id, a.token, a.expiresAt) "
            + " FROM AuthToken a "
            + " WHERE a.user.id = :userId ")
    // @formatter:on
    List<AuthTokenDto> getAuthTokensForUser(@Param("userId") Long userId);

 // @formatter:off
    @Query("SELECT a.user "
            + " FROM AuthToken a "
            + " WHERE a.token = :token"
            + " AND (a.expiresAt is NULL OR a.expiresAt > NOW()) ")
    // @formatter:on
    User getActiveTokenUserOwner(@Param("token") String token);

    @Modifying
    @Query("DELETE FROM AuthToken a WHERE a.user.id = :userId")
    void deleteUserTokens(@Param("userId") Long userId);
}
