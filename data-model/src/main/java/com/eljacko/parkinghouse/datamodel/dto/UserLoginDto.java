package com.eljacko.parkinghouse.datamodel.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class UserLoginDto {
    private Long userId;
    private String email;
    private String password;

    public UserLoginDto() {
    }

    public UserLoginDto(final Long userId, final String email,
            final String password) {
        super();
        this.userId = userId;
        this.email = email;
        this.password = password;
    }

}
