package com.eljacko.parkinghouse.datamodel.repository;

import com.eljacko.parkinghouse.datamodel.entity.Parking;
import com.eljacko.parkinghouse.datamodel.entity.user.Customer;
import com.eljacko.parkinghouse.datamodel.entity.user.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ParkingRepository extends JpaRepository<Parking, Long> {

    Optional<Parking> findById(Long id);

    Parking getOne(Long id);

    Parking save(Parking entity);

    List<Parking> findByEndTimeBetweenAndCustomerAndVendor(Date from, Date till,
                                                           Customer customer, Vendor vendor);

}
