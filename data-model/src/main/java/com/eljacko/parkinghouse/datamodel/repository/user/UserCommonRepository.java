package com.eljacko.parkinghouse.datamodel.repository.user;

import com.eljacko.parkinghouse.datamodel.entity.user.User;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.util.Optional;

@NoRepositoryBean
public interface UserCommonRepository<T extends User> extends Repository<T, Long> {

    Optional<T> findById(Long id);

    T getOne(Long id);

    T save(User entity);


}
