package com.eljacko.parkinghouse.datamodel.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings({ "checkstyle:magicnumber" })
public final class ForeignEntityBuilder {
 // @formatter:off
    public static final String ALPHABET =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
 // @formatter:on
    public static Long getRunId() {
        return new Long(new SimpleDateFormat("yyyMMddHHmmssS").format(new Date()));
    }

    private ForeignEntityBuilder() {
        throw new UnsupportedOperationException(
                "This is a utils class and cannot be instantiated");
    }
}
