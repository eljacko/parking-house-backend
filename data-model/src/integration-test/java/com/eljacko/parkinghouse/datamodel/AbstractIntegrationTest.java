package com.eljacko.parkinghouse.datamodel;

import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;


import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@SuppressWarnings({ "checkstyle:magicnumber" })
public abstract class AbstractIntegrationTest {
    public abstract TestEntityManager getEntityManager();

    protected final Long getRunId() {
        return new Long(new SimpleDateFormat("yyyMMddHHmmssS").format(new Date()));
    }

    protected final void flushAndClear() {
        getEntityManager().flush();
        getEntityManager().clear();
    }

}
