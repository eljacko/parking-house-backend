package com.eljacko.parkinghouse.webapi.util;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public final class UnitTestHelper {
    private UnitTestHelper() {
        throw new UnsupportedOperationException(
                "This is a utility class and cannot be instantiated");
    }

    public static Timestamp currentTimeAsTimestamp() {
        LocalDateTime today = LocalDateTime.now();
        return Timestamp.valueOf(today);
    }
}
