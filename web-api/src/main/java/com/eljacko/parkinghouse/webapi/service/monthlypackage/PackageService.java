package com.eljacko.parkinghouse.webapi.service.monthlypackage;

import com.eljacko.parkinghouse.datamodel.entity.product.MonthlyPackage;
import com.eljacko.parkinghouse.datamodel.entity.user.Customer;
import com.eljacko.parkinghouse.datamodel.entity.user.Vendor;
import com.eljacko.parkinghouse.webapi.dto.monthlypackage.PackageData;
import com.eljacko.parkinghouse.webapi.dto.monthlypackage.PackageDataSaveResult;

public interface PackageService {

    MonthlyPackage getActivePackageWithVendor(Customer customer, Vendor vendor);

    void addPackageToUser(Long userId, Long packageId);

    PackageDataSaveResult save(PackageData packageData);

}
