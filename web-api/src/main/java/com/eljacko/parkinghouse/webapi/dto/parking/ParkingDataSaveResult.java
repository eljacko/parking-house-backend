package com.eljacko.parkinghouse.webapi.dto.parking;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ParkingDataSaveResult {
    private ParkingData parkingData;
}
