package com.eljacko.parkinghouse.webapi.service.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public final class Utils {
    private Utils() {
        throw new UnsupportedOperationException(
                "This is a utility class and cannot be instantiated");
    }

    /**
     * Takes a string, and converts it to hashed string using MD5 algorithm.
     *
     * @param input
     *            string to be hashed
     * @return generated MD5 hashed value of the input
     */
    @SuppressWarnings("checkstyle:magicnumber")
    public static String md5(final String input) {
        if (input == null) {
            return null;
        }
        String hashedData = null;
        try {
            // Create MessageDigest object for MD5
            MessageDigest digest = MessageDigest.getInstance("MD5");

            // Update input string in message digest
            digest.update(input.getBytes(StandardCharsets.UTF_8), 0, input.length());

            // Converts message digest value in base 16 (hex)
            hashedData = new BigInteger(1, digest.digest()).toString(16);
            int leadingZeroesCount = Math.max(32 - hashedData.length(), 0);
            String leadingZeroes = String.join("", Collections.nCopies(leadingZeroesCount, "0"));
            hashedData = leadingZeroes + hashedData;
        } catch (Exception e) {
            log.error("Trying to convert string ({}) to MD5 got exception:{}", input,
                    e.getMessage(), e);
        }
        return hashedData;
    }

    @SuppressWarnings({ "checkstyle:magicnumber" })
    public static String hashToken(final String input) {
        String salt = "E89fe62ec86347c2b50f4A874bf6cc5e67b1CD579a9989a8bf7cb";
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update((input + salt).getBytes(StandardCharsets.UTF_8));
            byte[] messageDigestBytes = messageDigest.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte bytes : messageDigestBytes) {
                stringBuffer.append(String.format("%02x", bytes & 0xff));
            }

            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            log.error("Trying to generate token got exception:{}", input, e.getMessage(), e);
        }
        return null;
    }

    @SuppressWarnings({ "checkstyle:magicnumber" })
    public static String sha1(final String input) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
            messageDigest.update(input.getBytes(StandardCharsets.UTF_8));
            byte[] messageDigestBytes = messageDigest.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte bytes : messageDigestBytes) {
                stringBuffer.append(String.format("%02x", bytes & 0xff));
            }

            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            log.error("Trying to generate token got exception:{}", input, e.getMessage(), e);
        }
        return null;
    }

    public static <T> boolean isListContainsDuplicates(final List<T> list) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }

        if (getDistinctValuesList(list).size() < list.size()) {
            return true;
        }

        return false;
    }

    public static <T> List<T> getDistinctValuesList(final List<T> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }

        return list.stream().distinct().collect(Collectors.toList());
    }

    public static String getMapChecksum(final Map<String, ? extends Object> map) {
        if (map != null) {
            StringBuilder builder = new StringBuilder();
            ArrayList<String> keys = new ArrayList<>(map.keySet());
            keys.sort(Comparator.naturalOrder());
            for (final String key : keys) {
                builder.append(key);
                builder.append(map.get(key));
            }
            return Utils.sha1(builder.toString());
        } else {
            return null;
        }
    }
}
