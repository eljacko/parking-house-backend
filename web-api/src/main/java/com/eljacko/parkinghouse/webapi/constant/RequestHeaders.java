package com.eljacko.parkinghouse.webapi.constant;

public final class RequestHeaders {
    public static final String API_KEY = "Api-Key";
    public static final String TOKEN = "User-Token";

    private RequestHeaders() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
