package com.eljacko.parkinghouse.webapi.dto.parking;

import com.eljacko.parkinghouse.datamodel.constant.ValidationMessages;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class ParkingCommand {
    private Long id;
    private Long customerId;
    private Long vendorId;
    @NotNull(message = ValidationMessages.NOT_NULL)
    @NotBlank
    // START or END
    private String command;
}
