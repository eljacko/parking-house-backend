package com.eljacko.parkinghouse.webapi.controller.monthlypackage;

import com.eljacko.parkinghouse.webapi.constant.WebApiPaths;
import com.eljacko.parkinghouse.webapi.controller.AbstractController;
import com.eljacko.parkinghouse.webapi.dto.monthlypackage.PackageData;
import com.eljacko.parkinghouse.webapi.dto.monthlypackage.PackageDataSaveResult;
import com.eljacko.parkinghouse.webapi.dto.response.SimpleTokenResponse;
import com.eljacko.parkinghouse.webapi.service.auth.SessionUserPermissionValidator;
import com.eljacko.parkinghouse.webapi.service.monthlypackage.PackageService;
import com.eljacko.parkinghouse.webapi.service.util.DateUtilService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@Slf4j
@Getter
@RequiredArgsConstructor
@RequestMapping("/stateful/v1")
public class MonthlyPackageController extends AbstractController {
    private final SessionUserPermissionValidator sessionParkingPermissionValidator;
    private final PackageService packageService;
    private final DateUtilService dateUtilService;

    @PostMapping(WebApiPaths.PACKAGES)
    public HttpEntity<?> add(@RequestBody @Valid final PackageData packageData) {
        log.debug("Request to insert package {}", packageData);
        //sessionParkingPermissionValidator.validateVendorPermission(parkingData.getVendorId());

        PackageDataSaveResult result = packageService.save(packageData);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(result.getPackageData().getId()).toUri();
        log.debug("Parking data insert is done: parkingId:{}", result.getPackageData().getId());
        return ResponseEntity.created(location).body(result.getPackageData());
    }

    @PutMapping(WebApiPaths.PACKAGE)
    final HttpEntity<?> update(
            @PathVariable("userId") final Long userId,
            @RequestParam(value = "packageId", required = true) final Long packageId) {
        log.debug("Request to add package to user: userId:{}, packageId={}", userId, packageId);

        packageService.addPackageToUser(userId, packageId);
        SimpleTokenResponse response = new SimpleTokenResponse();
        response.setTimestampAsStr(dateUtilService.getInstantNow());
        log.debug("User package update is done: userId:{}", userId);
        return new ResponseEntity<SimpleTokenResponse>(response, HttpStatus.OK);
    }

}
