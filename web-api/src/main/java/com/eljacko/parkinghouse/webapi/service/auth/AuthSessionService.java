package com.eljacko.parkinghouse.webapi.service.auth;

import com.eljacko.parkinghouse.datamodel.dto.UserLoginDto;
import com.eljacko.parkinghouse.webapi.dto.auth.AuthData;

import javax.validation.constraints.NotNull;

public interface AuthSessionService {

    AuthData authenticate(String username, String password);

    /**
     * Find person without password by username.
     *
     * @param username
     * @return user login info found by username
     * @throws IllegalArgumentException
     *             if username is null
     * @throws com.eljacko.parkinghouse.webapi.exception.AuthorizationException
     *             if there is more than one user with the specified username or if user's password
     *             is defined in database
     */
    UserLoginDto getAuthUserWithoutPassword(@NotNull String username);

    /**
     * Saves user's password
     *
     * @param personId
     * @param plainPassword
     *            plain password that user want to use for login
     * @return the token for the user’s session
     * @throws IllegalArgumentException
     *             if personId or password is null
     */
    AuthData saveUserPassword(@NotNull Long personId, @NotNull String plainPassword);

}
