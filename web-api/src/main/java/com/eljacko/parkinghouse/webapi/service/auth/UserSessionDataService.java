package com.eljacko.parkinghouse.webapi.service.auth;

import com.eljacko.parkinghouse.webapi.dto.auth.AuthData;
import com.eljacko.parkinghouse.webapi.session.CurrentSessionUserInfo;

public interface UserSessionDataService {

    CurrentSessionUserInfo getUserInfo(AuthData authData);

    CurrentSessionUserInfo getUserInfo(CurrentSessionUserInfo currentInfo);

}
