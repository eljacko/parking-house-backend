package com.eljacko.parkinghouse.webapi.service.auth;

import com.eljacko.parkinghouse.datamodel.dto.AuthTokenDto;

import javax.validation.constraints.NotNull;
import java.util.Optional;

public interface AuthTokenService {

    /**
     * Creates new token (saves to database) or returns existing one for the user
     *
     * @param userId
     *            unique identifier of user who should be provided with token
     * @return user's token, or Optional#empty() if new token cannot be created
     * @throws IllegalArgumentException
     *             if userId is null
     */
    Optional<AuthTokenDto> provideToken(@NotNull Long userId);

    /**
     * Creates and saves new token for user
     *
     * @param userId
     *            unique identifier of user to whom add token
     * @param textForHash
     *            text that should be used generating token
     * @return saved token data
     * @throws IllegalArgumentException
     *             if userId is null
     */
    AuthTokenDto saveToken(@NotNull Long userId, String textForHash);

}
