package com.eljacko.parkinghouse.webapi.controller.auth;

import com.eljacko.parkinghouse.webapi.constant.WebApiPaths;
import com.eljacko.parkinghouse.webapi.controller.common.AbstractStatefulController;
import com.eljacko.parkinghouse.webapi.service.auth.SessionUserPermissionValidator;
import com.eljacko.parkinghouse.webapi.session.CurrentSessionData;
import com.eljacko.parkinghouse.webapi.session.CurrentSessionUserInfo;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Setter
@Getter
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/stateful/")
@SuppressWarnings("checkstyle:designforextension")
public class SessionUserController extends AbstractStatefulController {
    private final CurrentSessionData sessionData;
    private final SessionUserPermissionValidator sessionUserPermissionValidator;

    @GetMapping("v1" + WebApiPaths.ME)
    public HttpEntity<?> getUserSessionInfo() {
        log.debug("Request for user info in session");
        sessionUserPermissionValidator.validateUserIsLoggedIn();
        return new ResponseEntity<CurrentSessionUserInfo>(sessionData.getUserInfo(),
                HttpStatus.OK);
    }
}
