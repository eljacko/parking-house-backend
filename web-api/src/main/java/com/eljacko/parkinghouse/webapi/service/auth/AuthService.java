package com.eljacko.parkinghouse.webapi.service.auth;

import com.eljacko.parkinghouse.datamodel.dto.AuthTokenDto;

public interface AuthService {

    AuthTokenDto authenticate(String username, String password);

    AuthTokenDto authenticate(String username, String password, String apiKey);

}
