package com.eljacko.parkinghouse.webapi.session;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
@Component("sessionData")
public class CurrentSessionData implements SessionData, Serializable {
    private static final long serialVersionUID = 4961089215867746972L;
    private String token;
    private CurrentSessionUserInfo userInfo;
}
