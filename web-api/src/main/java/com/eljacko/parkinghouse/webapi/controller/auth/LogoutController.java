package com.eljacko.parkinghouse.webapi.controller.auth;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/stateful")
public class LogoutController {
    @SuppressWarnings({ "checkstyle:FinalParameters" })
    @PostMapping("/logout")
    public final HttpEntity<?> logout(HttpSession session) {
        session.invalidate();
        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }
}
