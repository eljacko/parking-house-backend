package com.eljacko.parkinghouse.webapi.session;

public interface SessionData {
    SessionUserInfo getUserInfo();

}
