package com.eljacko.parkinghouse.webapi.session;

import com.eljacko.parkinghouse.datamodel.entity.user.User;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class CurrentSessionUserInfo implements SessionUserInfo, Serializable {
    private static final long serialVersionUID = 5934904176083469432L;
    private CurrentSessionUser user;


    public CurrentSessionUserInfo() {
        super();
        user = new CurrentSessionUser();
    }

    public final void addUserData(final User usr) {
        user.setId(usr.getId());
        user.setName(usr.getName());
        user.setEmail(usr.getEmail());
    }

}
