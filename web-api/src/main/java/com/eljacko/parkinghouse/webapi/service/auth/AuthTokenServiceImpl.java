package com.eljacko.parkinghouse.webapi.service.auth;

import com.eljacko.parkinghouse.datamodel.dto.AuthTokenDto;
import com.eljacko.parkinghouse.datamodel.entity.AuthToken;
import com.eljacko.parkinghouse.datamodel.repository.AuthTokenRepository;
import com.eljacko.parkinghouse.datamodel.repository.UserRepository;
import com.eljacko.parkinghouse.webapi.service.util.Utils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
@Slf4j
@RequiredArgsConstructor
public class AuthTokenServiceImpl implements AuthTokenService {
    private final UserRepository userRepository;
    private final AuthTokenRepository authTokenRepository;

    private final int randomUpperBound = 5000;

    @Override
    @Transactional
    public Optional<AuthTokenDto> provideToken(@NotNull final Long userId) {
        Assert.notNull(userId, "userId must not be null");
        log.debug("get token for userId={}", userId);

        // get token or create new one if there is no
        List<AuthTokenDto> tokenList = authTokenRepository.getAuthTokensForUser(userId);
        if (CollectionUtils.isEmpty(tokenList)) {
            return Optional.of(saveToken(userId, Thread.currentThread().getName()));
        } else {
            if (tokenList.size() == 1) {
                return Optional.of(tokenList.get(0));
            } else {
                log.error("There are more than one token for user ({})", userId);
            }
        }
        return Optional.empty();

    }

    @Override
    @Transactional
    public AuthTokenDto saveToken(@NotNull final Long userId, final String textForHash) {
        Assert.notNull(userId, "User id must not be null");
        AuthToken newTokenDb = new AuthToken();
        newTokenDb.setUser(userRepository.getOne(userId));
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(userId);
        stringBuffer.append(textForHash);
        stringBuffer.append(Instant.now().getEpochSecond());
        stringBuffer.append(new Random().nextInt(randomUpperBound));
        newTokenDb.setToken(Utils.hashToken(stringBuffer.toString()));
        newTokenDb = authTokenRepository.save(newTokenDb);
        return new AuthTokenDto(newTokenDb.getId(), userId, newTokenDb.getToken(),
                newTokenDb.getExpiresAt());
    }

}
