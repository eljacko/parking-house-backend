package com.eljacko.parkinghouse.webapi.controller.parking;

import com.eljacko.parkinghouse.datamodel.utils.DateTimeUtils;
import com.eljacko.parkinghouse.webapi.constant.WebApiPaths;
import com.eljacko.parkinghouse.webapi.controller.AbstractController;
import com.eljacko.parkinghouse.webapi.dto.FieldError;
import com.eljacko.parkinghouse.webapi.dto.parking.ParkingData;
import com.eljacko.parkinghouse.webapi.dto.parking.ParkingDataSaveResult;
import com.eljacko.parkinghouse.webapi.dto.response.SimpleTokenResponse;
import com.eljacko.parkinghouse.webapi.exception.InvalidParameterException;
import com.eljacko.parkinghouse.webapi.exception.InvalidParametersException;
import com.eljacko.parkinghouse.webapi.service.auth.SessionUserPermissionValidator;
import com.eljacko.parkinghouse.webapi.service.parking.ParkingService;
import com.eljacko.parkinghouse.webapi.service.util.DateUtilService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
@Getter
@RequiredArgsConstructor
@RequestMapping("/stateful/v1")
@SuppressWarnings("checkstyle:designforextension")
public class ParkingController extends AbstractController {
    private final SessionUserPermissionValidator sessionParkingPermissionValidator;
    private final ParkingService parkingService;
    private final DateUtilService dateUtilService;

    @PostMapping(WebApiPaths.PARKINGS)
    public HttpEntity<?> add(@RequestBody @Valid final ParkingData parkingData) {
        log.debug("Request to insert parking {}", parkingData);
        //sessionParkingPermissionValidator.validateVendorPermission(parkingData.getVendorId());

        ParkingDataSaveResult result = save(null, parkingData);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(result.getParkingData().getId()).toUri();
        log.debug("Parking data insert is done: parkingId:{}", result.getParkingData().getId());
        return ResponseEntity.created(location).body(result.getParkingData());
    }

    @PutMapping(WebApiPaths.PARKING)
    final HttpEntity<?> update(@PathVariable("parkingId") final Long parkingId,
                               @RequestBody @Valid final ParkingData parkingData) {
        log.debug("Request to update parking: parkingId:{}, data={}", parkingId, parkingData);
        //sessionParkingPermissionValidator.validateVendorPermission(parkingData.getVendorId());

        save(parkingId, parkingData);
        SimpleTokenResponse response = new SimpleTokenResponse();
        response.setTimestampAsStr(dateUtilService.getInstantNow());
        log.debug("Parking data update is done: parkingId:{}", parkingId);
        return new ResponseEntity<SimpleTokenResponse>(response, HttpStatus.OK);
    }

    private ParkingDataSaveResult save(final Long parkingId, final ParkingData parkingData) {
        ParkingDataSaveResult result = null;
        parkingData.setId(parkingId);
        if (isValidParkingData(parkingData)) {
            result = parkingService.save(parkingData);
        }
        return result;
    }

    private boolean isValidParkingData(final ParkingData parkingData) {
        List<FieldError> fieldErrors = new ArrayList<>();
        if (parkingData.getId() == null
                && (parkingData == null || parkingData.getCustomerId() == null)) {
            fieldErrors.add(new FieldError("customerId",
                    "Customer ID should not be empty."));
        }
        if (parkingData.getId() == null
                && (parkingData == null || parkingData.getVendorId() == null)) {
            fieldErrors.add(new FieldError("vendorId",
                    "Vendor ID should not be empty."));
        }
        try {
            DateTimeUtils.parseRFC3339(parkingData.getStartTime());
        } catch (DateTimeParseException e) {
            log.error("Exception", e);
            throw new InvalidParameterException("startTime", "Wrong date format");
        }
        try {
            DateTimeUtils.parseRFC3339(parkingData.getEndTime());
        } catch (DateTimeParseException e) {
            log.error("Exception", e);
            throw new InvalidParameterException("endTime", "Wrong date format");
        }
        if (CollectionUtils.isEmpty(fieldErrors)) {
            return true;
        }
        throw new InvalidParametersException(fieldErrors);
    }
}
