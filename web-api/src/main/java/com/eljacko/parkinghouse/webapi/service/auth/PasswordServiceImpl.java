package com.eljacko.parkinghouse.webapi.service.auth;

import com.eljacko.parkinghouse.datamodel.entity.user.User;
import com.eljacko.parkinghouse.datamodel.repository.UserRepository;
import com.eljacko.parkinghouse.webapi.service.util.BCrypt;
import com.eljacko.parkinghouse.webapi.service.util.DateUtilService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
@SuppressWarnings("checkstyle:designforextension")
public class PasswordServiceImpl implements PasswordService {

    private final DateUtilService dateUtilService;

    private final UserRepository userRepository;

    public static final int PASSWORD_SALT_ROUNDS = 12;

    @Override
    public final String getHashedPassword(final String plainPassword) {
        Optional<String> password = Optional.ofNullable(plainPassword);
        if (password.isPresent() && password.get().length() > 0) {
            String salt = BCrypt.gensalt(PASSWORD_SALT_ROUNDS);
            return BCrypt.hashpw(password.get(), salt);
        }

        return null;
    }

    @Override
    @Transactional
    public void setUserPassword(@NotNull final User userDb,
            @NotNull final String plainPassword) {
        Assert.notNull(userDb, "User must not be null");
        Assert.notNull(plainPassword, "Password must not be null");

        savePassword(userDb, plainPassword);

    }

    private void savePassword(@NotNull final User userDb, final String plainPassword) {
        // change user's password
        if (plainPassword != null) {
            log.debug("Set new password for user={}", userDb.getId());
            userDb.setPassword(getHashedPassword(plainPassword));
            userDb.setPasswordCreated(dateUtilService.getCurrentTimeAsTimestamp());
        } else {
            log.debug("Unset password for user={}", userDb.getId());
            userDb.setPassword(null);
            userDb.setPasswordCreated(null);
        }
        userRepository.save(userDb);
    }

}
