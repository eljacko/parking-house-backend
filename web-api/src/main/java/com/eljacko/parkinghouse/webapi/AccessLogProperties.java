package com.eljacko.parkinghouse.webapi;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "application.accesslog")
@SuppressWarnings({ "checkstyle:magicnumber" })
public class AccessLogProperties {
    /**
     * Enable access log.
     */
    private boolean enabled = false;

    /**
     * Format pattern for access logs.
     */
    private String pattern = "common";
}
