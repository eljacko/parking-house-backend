package com.eljacko.parkinghouse.webapi;

import com.eljacko.parkinghouse.datamodel.utils.CurrentAuditor;
import com.eljacko.parkinghouse.webapi.constant.RequestHeaders;
import com.eljacko.parkinghouse.webapi.session.SessionData;
import com.eljacko.parkinghouse.webapi.session.SessionUser;
import com.eljacko.parkinghouse.webapi.session.SessionUserInfo;
import org.springframework.data.domain.AuditorAware;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public final Optional<String> getCurrentAuditor() {
        Optional<HttpServletRequest> request = Optional
                .ofNullable(
                        (ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .map(ServletRequestAttributes::getRequest);
        if (request.isPresent()) {
            Optional<String> auditor = Optional.ofNullable(request.get().getSession())
                    .map(sess -> (SessionData) sess.getAttribute("scopedTarget.sessionData"))
                    .map(SessionData::getUserInfo).map(SessionUserInfo::getUser)
                    .map(SessionUser::getAuditor).map(CurrentAuditor::toString);
            if (!auditor.isPresent()) {
                auditor = Optional.ofNullable(request.get().getHeader(RequestHeaders.API_KEY))
                        .map(ak -> new CurrentAuditor("user", null, "apiKey=" + ak))
                        .map(ca -> ca.toString());
            }
            if (auditor.isPresent()) {
                return auditor;
            }
        }
        return Optional.of("Web-api");
        // Can use Spring Security to return currently logged in user
        // return ((User)
        // SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()
    }
}
