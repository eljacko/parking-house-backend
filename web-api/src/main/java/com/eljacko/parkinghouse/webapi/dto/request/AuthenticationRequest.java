package com.eljacko.parkinghouse.webapi.dto.request;

import javax.validation.constraints.NotBlank;

import com.eljacko.parkinghouse.datamodel.constant.ValidationMessages;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class AuthenticationRequest {
    @NotBlank(message = ValidationMessages.NOT_NULL)
    private String username;
    @NotBlank(message = ValidationMessages.NOT_NULL)
    private String password;
}
