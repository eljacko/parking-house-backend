package com.eljacko.parkinghouse.webapi.service.auth;

import com.eljacko.parkinghouse.datamodel.entity.user.User;

import javax.validation.constraints.NotNull;

public interface PasswordService {

    /**
     * Creates a new password hash using one-way hashing algorithm.
     *
     * @param plainPassword
     *            the password to be hashed
     * @return Returns the hashed password, or NULL on empty passwordOrigin.
     */

    String getHashedPassword(String plainPassword);

    /**
     * Saves person's password and adds existing password to history.
     *
     * @param userDb
     *            person entity
     * @param plainPassword
     *            plain password that person will use for login
     * @throws IllegalArgumentException
     *             if personDb or plainPassword is null
     */
    void setUserPassword(@NotNull User userDb, @NotNull String plainPassword);

}
