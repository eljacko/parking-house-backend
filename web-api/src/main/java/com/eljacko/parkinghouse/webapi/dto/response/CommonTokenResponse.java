package com.eljacko.parkinghouse.webapi.dto.response;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.Instant;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public abstract class CommonTokenResponse {
    private String timestamp;
    // private String token;

    public final void setTimestampAsStr(final Instant time) {
        setTimestamp(time.toString());
    }
}
