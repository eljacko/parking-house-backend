package com.eljacko.parkinghouse.webapi.dto.parking;

import com.eljacko.parkinghouse.datamodel.constant.ValidationMessages;
import com.eljacko.parkinghouse.datamodel.entity.Parking;
import com.eljacko.parkinghouse.datamodel.utils.DateTimeUtils;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class ParkingData {
    private Long id;
    private Long customerId;
    private Long vendorId;
    @NotNull(message = ValidationMessages.NOT_NULL)
    @NotBlank
    private String startTime;
    @NotNull(message = ValidationMessages.NOT_NULL)
    @NotBlank
    private String endTime;

    public ParkingData(final Parking parking) {
        super();
        setId(parking.getId());
        setCustomerId(parking.getCustomer().getId());
        setVendorId(parking.getVendor().getId());
        setStartTime(DateTimeUtils.formatRFC3339(parking.getStartTime()));
        setEndTime(DateTimeUtils.formatRFC3339(parking.getEndTime()));
    }

}
