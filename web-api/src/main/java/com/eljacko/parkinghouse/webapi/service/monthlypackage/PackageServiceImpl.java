package com.eljacko.parkinghouse.webapi.service.monthlypackage;

import com.eljacko.parkinghouse.datamodel.entity.product.MonthlyPackage;
import com.eljacko.parkinghouse.datamodel.entity.product.ParkingFee;
import com.eljacko.parkinghouse.datamodel.entity.product.ProductInfo;
import com.eljacko.parkinghouse.datamodel.entity.user.Customer;
import com.eljacko.parkinghouse.datamodel.entity.user.CustomerPackage;
import com.eljacko.parkinghouse.datamodel.entity.user.Vendor;
import com.eljacko.parkinghouse.datamodel.repository.CustomerRepository;
import com.eljacko.parkinghouse.datamodel.repository.VendorRepository;
import com.eljacko.parkinghouse.datamodel.repository.product.CustomerPackageRepository;
import com.eljacko.parkinghouse.datamodel.repository.product.MonthlyPackageRepository;
import com.eljacko.parkinghouse.datamodel.repository.product.ParkingFeeRepository;
import com.eljacko.parkinghouse.datamodel.repository.product.ProductInfoRepository;
import com.eljacko.parkinghouse.webapi.dto.monthlypackage.PackageData;
import com.eljacko.parkinghouse.webapi.dto.monthlypackage.PackageDataSaveResult;
import com.eljacko.parkinghouse.webapi.exception.NotFoundException;
import com.eljacko.parkinghouse.webapi.service.util.DateUtilService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
@RequiredArgsConstructor
@Log4j2
public class PackageServiceImpl implements PackageService {
    private final CustomerRepository customerRepository;
    private final VendorRepository vendorRepository;
    private final CustomerPackageRepository customerPackageRepository;
    private final MonthlyPackageRepository monthlyPackageRepository;
    private final ParkingFeeRepository parkingFeeRepository;
    private final ProductInfoRepository productInfoRepository;
    private final DateUtilService dateUtilService;

    @Override
    public PackageDataSaveResult save(PackageData packageData) {
        Assert.notNull(packageData, "Package data must not be null");
        MonthlyPackage monthlyPackageDb = new MonthlyPackage();
        monthlyPackageDb.setCurrency("EUR");
        monthlyPackageDb.setPrice(packageData.getPrice());
        Vendor vendorDb = vendorRepository.findById(packageData.getVendorId()).orElseThrow(() -> {
            log.debug("There is no vendor with id={}", packageData.getVendorId());
            return new NotFoundException("There is no vendor with such id");
        });
        monthlyPackageDb.setVendor(vendorDb);
        monthlyPackageRepository.save(monthlyPackageDb);
        ProductInfo productInfoDb = new ProductInfo();
        productInfoDb.setProduct(monthlyPackageDb);
        productInfoDb.setMaximumInvoice(packageData.getMaximumInvoice());
        ParkingFee parkingFeeDb;
        if (packageData.getParkingFeeId() != null) {
            parkingFeeDb = parkingFeeRepository.findById(packageData.getParkingFeeId())
                    .orElseThrow(() -> {
                log.debug("There is no parking fee with id={}",
                        packageData.getParkingFeeId());
                return new NotFoundException("There is no parking fee  with such id");
            });

        } else {
            parkingFeeDb = new ParkingFee();
            parkingFeeDb.setVendor(vendorDb);
            parkingFeeDb.setPrice(packageData.getParkingFee());
            parkingFeeDb.setCurrency("EUR");
            parkingFeeRepository.save(parkingFeeDb);
        }
        productInfoRepository.save(productInfoDb);
        productInfoDb.setParkingFee(parkingFeeDb);

        monthlyPackageDb.setProductInfo(productInfoDb);
        PackageDataSaveResult result = new PackageDataSaveResult();
        result.setPackageData(new PackageData(monthlyPackageDb));
        return result;
    }

    @Override
    public MonthlyPackage getActivePackageWithVendor(Customer customer, Vendor vendor) {
        List<CustomerPackage> customerPackages = customerPackageRepository
                .findCustomerPackagesByEndDateIsNullAndCustomer(customer);
        CustomerPackage customerPackageDb = customerPackages.stream()
                .filter(cp -> cp.getMonthlyPackage().getVendor().equals(vendor))
                .findAny().orElseGet(() -> null);
        if (customerPackageDb != null) {
            return customerPackageDb.getMonthlyPackage();
        }
        return monthlyPackageRepository.getDefaultPackage(vendor.getId());
    }

    @Override
    public void addPackageToUser(Long customerId, Long packageId) {
        Customer customerDb = customerRepository.findById(customerId).orElseThrow(() -> {
            log.debug("There is no customer with id={}", customerId);
            return new NotFoundException("There is no customer with such id");
        });
        MonthlyPackage packageDb = monthlyPackageRepository.findById(packageId).orElseThrow(() -> {
            log.debug("There is no package with id={}", packageId);
            return new NotFoundException("There is no package with such id");
        });
        List<CustomerPackage> customerPackages = customerPackageRepository
                .findCustomerPackagesByEndDateIsNullAndCustomer(customerDb);
        CustomerPackage currentCustomerPackageDb = customerPackages.stream()
                .filter(cp -> cp.getMonthlyPackage().getVendor().getId().equals(packageDb
                        .getVendor().getId()))
                .findAny().orElseGet(() -> null);
        if (currentCustomerPackageDb != null) {
            currentCustomerPackageDb.setEndDate(dateUtilService.getCurrentTimeAsTimestamp());
            customerPackageRepository.save(currentCustomerPackageDb);
        }
        CustomerPackage customerPackageDb = new CustomerPackage(customerDb, packageDb);
        customerPackageDb.setStartDate(dateUtilService.getCurrentTimeAsTimestamp());
        customerPackageRepository.save(customerPackageDb);
    }
}
