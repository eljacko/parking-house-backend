package com.eljacko.parkinghouse.webapi.session;

import com.eljacko.parkinghouse.datamodel.utils.CurrentAuditor;

public interface SessionUser {
    CurrentAuditor getAuditor();
}
