package com.eljacko.parkinghouse.webapi;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SuppressWarnings("checkstyle:designforextension")
@Configuration
public class WebSecurityConfig implements WebMvcConfigurer {

    private static final int CORS_MAX_AGE = 3600;

    @Getter
    @Value("${web-api.frontend.public-url:}")
    private String frontendPublicUrl = null;


    public void addCorsMappings(final CorsRegistry registry) {
         registry
            //.addMapping("/**").allowedOrigins(frontendPublicUrl)
            .addMapping("/stateful/**").allowedOrigins(frontendPublicUrl)
            .maxAge(CORS_MAX_AGE)
            .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
            .allowCredentials(true);
    }
}
