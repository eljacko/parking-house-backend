package com.eljacko.parkinghouse.webapi;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Slf4j
@Configuration
@EnableAspectJAutoProxy
@ComponentScan("com.eljacko.parkinghouse.datamodel")
@SuppressWarnings("checkstyle:designforextension")
public class ApplicationConfig {

    @Bean
    public GsonJsonParser gsonJsonParser() {
        return new GsonJsonParser();
    }

    @Bean
    public Gson gson() {
        return new Gson();
    }
 }
