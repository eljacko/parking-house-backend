package com.eljacko.parkinghouse.webapi.service.auth;

import com.eljacko.parkinghouse.webapi.exception.PermissionControlException;
import com.eljacko.parkinghouse.webapi.exception.UnauthorizedException;
import com.eljacko.parkinghouse.webapi.session.CurrentSessionData;
import com.eljacko.parkinghouse.webapi.session.CurrentSessionUser;
import com.eljacko.parkinghouse.webapi.session.CurrentSessionUserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class SessionUserPermissionValidatorImpl implements SessionUserPermissionValidator {
    private final CurrentSessionData sessionData;

    @Override
    public final boolean isLoggedIn() {
        Optional<CurrentSessionUser> userIdOpt = Optional.ofNullable(sessionData)
                .map(CurrentSessionData::getUserInfo).map(CurrentSessionUserInfo::getUser)
                .filter(CurrentSessionUser::isLoggedIn);
        log.debug("------------sessionData {}", sessionData.toString());
        log.debug("------------userIdOpt.isPresent() {}", userIdOpt.isPresent());
        return userIdOpt.isPresent();
    }

    @Override
    public final boolean validateUserIsLoggedIn() {
        if (!isLoggedIn()) {
            throw new UnauthorizedException();
        }
        return true;
    }

    @Override
    public boolean validateVendorPermission(@NotNull final Long vendorId) {
        Assert.notNull(vendorId, "Vendor id must not be null");
        if (!isLoggedIn()) {
            throw new UnauthorizedException();
        }
        // just a quick "permissions handling", that checks only vendor ID
        boolean isSessionVendor = vendorId.equals(sessionData.getUserInfo().getUser().getId());
        if (!isSessionVendor) {
            throw new PermissionControlException();
        }
        return true;
    }
}
