package com.eljacko.parkinghouse.webapi.constant;

public final class WebApiPaths {

    public static final String INVOICE = "/invoice";
    public static final String PACKAGES = "/packages";
    public static final String PACKAGE = "/package/{userId}";
    public static final String PARKINGS = "/parkings";
    public static final String PARKING = "/parking/{parkingId}";
    public static final String USERS = "/users";
    public static final String USER = "/user/{userId}";


    // @formatter:on
    /**
     * {@value #ME}
     */
    public static final String ME = "/me";
    // @formatter:off
    /** {@value #AUTHENTICATE} */
    public static final String AUTHENTICATE = "/authenticate";
    /** {@value #AUTHENTICATE_PASSWORD} */
    public static final String AUTHENTICATE_PASSWORD = "/password/authenticate";


    private WebApiPaths() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
