package com.eljacko.parkinghouse.webapi.dto.auth;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class AuthData {
    private String token;
    /**
     * Unique identifier of user
     */
    private Long userId;

    public AuthData(final String token, final Long userId) {
        super();
        this.token = token;
        this.userId = userId;
    }

    public AuthData(final String token) {
        super();
        this.token = token;
    }

}
