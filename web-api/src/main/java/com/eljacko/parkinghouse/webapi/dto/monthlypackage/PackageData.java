package com.eljacko.parkinghouse.webapi.dto.monthlypackage;

import com.eljacko.parkinghouse.datamodel.constant.ValidationMessages;
import com.eljacko.parkinghouse.datamodel.entity.product.MonthlyPackage;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class PackageData {
    private Long id;
    @NotNull(message = ValidationMessages.NOT_NULL)
    private Long vendorId;
    private Integer maximumInvoice;
    @NotNull(message = ValidationMessages.NOT_NULL)
    private Integer price;
    private Integer parkingFee;
    private Long parkingFeeId;
    private boolean defaultPackage;


    public PackageData(final MonthlyPackage monthlyPackage) {
        super();
        setId(monthlyPackage.getId());
        setVendorId(monthlyPackage.getVendor().getId());
        setPrice(monthlyPackage.getPrice());
        setParkingFee(monthlyPackage.getProductInfo().getParkingFee().getPrice());
        setMaximumInvoice(monthlyPackage.getProductInfo().getMaximumInvoice());
        setDefaultPackage(monthlyPackage.getProductInfo().isDefaultPackage());
    }
}
