package com.eljacko.parkinghouse.webapi.controller.user;

import com.eljacko.parkinghouse.webapi.constant.WebApiPaths;
import com.eljacko.parkinghouse.webapi.controller.common.AbstractStatefulController;
import com.eljacko.parkinghouse.webapi.dto.FieldError;
import com.eljacko.parkinghouse.webapi.dto.response.SimpleTokenResponse;
import com.eljacko.parkinghouse.webapi.dto.user.UserData;
import com.eljacko.parkinghouse.webapi.dto.user.UserDataSaveResult;
import com.eljacko.parkinghouse.webapi.exception.InvalidParametersException;
import com.eljacko.parkinghouse.webapi.service.auth.SessionUserPermissionValidator;
import com.eljacko.parkinghouse.webapi.service.user.UserService;
import com.eljacko.parkinghouse.webapi.service.util.DateUtilService;
import com.eljacko.parkinghouse.webapi.session.CurrentSessionData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
@Getter
@RequiredArgsConstructor
@RequestMapping("/stateful/v1")
@SuppressWarnings("checkstyle:designforextension")
public class UserController extends AbstractStatefulController {
    private final UserService userService;
    private final CurrentSessionData sessionData;
    private final SessionUserPermissionValidator sessionUserPermissionValidator;
    private final DateUtilService dateUtilService;

    @GetMapping(WebApiPaths.USER)
    public HttpEntity<?> get(@PathVariable("userId") final Long userId) {
        log.debug("Request to get user id:{}", userId);
        // not implementing any rules on adding new users
        // get user information
        UserData userData = userService.getUserData(userId);
        log.debug("Response for getting user id:{}", userId);
        return new ResponseEntity<UserData>(userData, HttpStatus.OK);
    }

    @PostMapping(WebApiPaths.USERS)
    public HttpEntity<?> add(@RequestBody @Valid final UserData userData) {
        log.debug("Request to insert user {}", userData);

        UserDataSaveResult result = save(null, userData);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(result.getUserData().getId()).toUri();
        log.debug("User data insert is done: userId:{}", result.getUserData().getId());
        return ResponseEntity.created(location).body(result.getUserData());
    }

    @PutMapping(WebApiPaths.USER)
    final HttpEntity<?> update(@PathVariable("userId") final Long userId,
            @RequestBody @Valid final UserData userData) {
        log.debug("Request to update user: userId:{}, data={}", userId, userData);

        save(userId, userData);
        SimpleTokenResponse response = new SimpleTokenResponse();
        response.setTimestampAsStr(dateUtilService.getInstantNow());
        log.debug("User data update is done: userId:{}", userId);
        return new ResponseEntity<SimpleTokenResponse>(response, HttpStatus.OK);
    }

    private UserDataSaveResult save(final Long userId, final UserData userData) {
        UserDataSaveResult result = null;
        userData.setId(userId);
        if (isValidUserData(userData)) {
            result = userService.save(userData);
        }
        return result;
    }

    private boolean isValidUserData(final UserData userData) {
        List<FieldError> fieldErrors = new ArrayList<>();
        if (userData == null || userData.getName() == null) {
            fieldErrors.add(new FieldError("name", "Name should not be empty."));
        } else {
            userData.setName(userData.getName().trim());
            if (StringUtils.isEmpty(userData.getName()) || userData.getName().length() < 2) {
                fieldErrors.add(new FieldError("name", "Min length 2 characters"));
            }
        }
        if (CollectionUtils.isEmpty(fieldErrors)) {
            return true;
        }
        throw new InvalidParametersException(fieldErrors);
    }

}
