package com.eljacko.parkinghouse.webapi.dto.monthlypackage;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class PackageDataSaveResult {
    private PackageData packageData;
}
