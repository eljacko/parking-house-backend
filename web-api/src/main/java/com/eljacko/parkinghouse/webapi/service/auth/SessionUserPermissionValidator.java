package com.eljacko.parkinghouse.webapi.service.auth;

import javax.validation.constraints.NotNull;

public interface SessionUserPermissionValidator {

    boolean isLoggedIn();

    boolean validateUserIsLoggedIn();

    boolean validateVendorPermission(@NotNull Long vendorId);

}
