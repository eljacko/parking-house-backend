package com.eljacko.parkinghouse.webapi.controller.auth;

import com.eljacko.parkinghouse.datamodel.dto.UserLoginDto;
import com.eljacko.parkinghouse.webapi.constant.WebApiPaths;
import com.eljacko.parkinghouse.webapi.controller.common.AbstractStatefulController;
import com.eljacko.parkinghouse.webapi.dto.auth.AuthData;
import com.eljacko.parkinghouse.webapi.dto.request.AuthenticationRequest;
import com.eljacko.parkinghouse.webapi.dto.response.AuthenticationResponse;
import com.eljacko.parkinghouse.webapi.exception.AuthorizationException;
import com.eljacko.parkinghouse.webapi.service.auth.AuthSessionService;
import com.eljacko.parkinghouse.webapi.service.auth.UserSessionDataService;
import com.eljacko.parkinghouse.webapi.service.util.DateUtilService;
import com.eljacko.parkinghouse.webapi.session.CurrentSessionData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@Getter
@Setter
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/stateful/v1/")
@SuppressWarnings("checkstyle:designforextension")
public class LoginController extends AbstractStatefulController {
    private final AuthSessionService authService;
    private final UserSessionDataService userSessionDataService;
    private final CurrentSessionData sessionData;
    private final DateUtilService dateUtilService;
    private final HttpSession httpSession;
    private final HttpServletRequest httpServletRequest;

    @PostMapping(WebApiPaths.AUTHENTICATE)
    public HttpEntity<?> authenticate(
            @RequestBody @Valid final AuthenticationRequest userCredentials) {
        log.debug("authenticate username={}", userCredentials.getUsername());
        AuthData tokenObj = authService.authenticate(userCredentials.getUsername(),
                userCredentials.getPassword());
        return putDataToSession(tokenObj, userCredentials.getUsername());
    }

    @PostMapping(WebApiPaths.AUTHENTICATE_PASSWORD)
    public HttpEntity<?> savePasswordAndAuthenticate(
            @RequestBody @Valid final AuthenticationRequest userCredentials) {
        log.debug("save password for username={}", userCredentials.getUsername());
        UserLoginDto userLogin = authService
                .getAuthUserWithoutPassword(userCredentials.getUsername());
        if (userLogin.getUserId() != null) {
            AuthData tokenObj = authService
                        .saveUserPassword(userLogin.getUserId(), userCredentials.getPassword());
            return putDataToSession(tokenObj, userCredentials.getUsername());

        } else {
            throw new AuthorizationException();
        }
    }

    private HttpEntity<?> putDataToSession(final AuthData tokenObj, final String username) {
        getHttpSession().invalidate();
        getHttpServletRequest().getSession(true);
        sessionData.setToken(tokenObj.getToken());
        sessionData.setUserInfo(
                userSessionDataService.getUserInfo(tokenObj));
        AuthenticationResponse response = new AuthenticationResponse();
        response.setToken("OK");
        response.setTimestampAsStr(getDateUtilService().getInstantNow());
        log.debug("Username={} is authenticated;", username);
        return new ResponseEntity<AuthenticationResponse>(response, HttpStatus.OK);
    }

}
