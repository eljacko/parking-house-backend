package com.eljacko.parkinghouse.webapi.service.parking;

import com.eljacko.parkinghouse.datamodel.entity.Parking;
import com.eljacko.parkinghouse.datamodel.repository.CustomerRepository;
import com.eljacko.parkinghouse.datamodel.repository.ParkingRepository;
import com.eljacko.parkinghouse.datamodel.repository.VendorRepository;
import com.eljacko.parkinghouse.datamodel.utils.DateTimeUtils;
import com.eljacko.parkinghouse.webapi.dto.parking.ParkingCommand;
import com.eljacko.parkinghouse.webapi.dto.parking.ParkingData;
import com.eljacko.parkinghouse.webapi.dto.parking.ParkingDataSaveResult;
import com.eljacko.parkinghouse.webapi.exception.NotFoundException;
import com.eljacko.parkinghouse.webapi.service.util.DateUtilService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Service
@Slf4j
@RequiredArgsConstructor
@SuppressWarnings("checkstyle:designforextension")
public class ParkingServiceImpl implements ParkingService {
    private final ParkingRepository parkingRepository;
    private final CustomerRepository customerRepository;
    private final VendorRepository vendorRepository;
    private final DateUtilService dateUtilService;

    @Override
    @Transactional(readOnly = true)
    public ParkingData getParkingData(Long parkingId) {
        Assert.notNull(parkingId, "Parking id must not be null");
        Parking parkingDb = parkingRepository.findById(parkingId)
                .orElseThrow(() -> new NotFoundException());
        return new ParkingData(parkingDb);
    }

    @Override
    @Transactional
    public ParkingDataSaveResult save(final ParkingData parkingData) {
        Assert.notNull(parkingData, "Parking data must not be null");
        log.debug("Save parking data id={}", parkingData.getId());
        Parking parkingDb;
        if (parkingData.getId() == null) {
            parkingDb = new Parking();
            parkingDb.setCustomer(customerRepository.findById(parkingData.getCustomerId())
                    .orElseThrow(() -> new IllegalArgumentException()));
            parkingDb.setVendor(vendorRepository.findById(parkingData.getVendorId())
                    .orElseThrow(() -> new IllegalArgumentException()));
        } else {
            parkingDb = parkingRepository.findById(parkingData.getId())
                    .orElseThrow(() -> new NotFoundException());
        }

        parkingDb.setStartTime(DateTimeUtils.parseRFC3339(parkingData.getStartTime()));
        parkingDb.setEndTime(DateTimeUtils.parseRFC3339(parkingData.getEndTime()));
        parkingDb = parkingRepository.save(parkingDb);

        ParkingDataSaveResult result = new ParkingDataSaveResult();
        result.setParkingData(new ParkingData(parkingDb));
        return result;
    }


    @Override
    public ParkingDataSaveResult processCommand(ParkingCommand parkingData) {
        // todo
        // start and end parking
        return null;
    }


}
