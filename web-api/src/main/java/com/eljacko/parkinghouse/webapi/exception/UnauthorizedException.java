package com.eljacko.parkinghouse.webapi.exception;

@SuppressWarnings({ "checkstyle:FinalParameters" })
public class UnauthorizedException extends RuntimeException {
    private static final long serialVersionUID = 4076119488371982779L;

    public UnauthorizedException() {
        super();
    }

    public UnauthorizedException(final String message) {
        super(message);
    }

    public UnauthorizedException(final String message, Throwable cause) {
        super(message, cause);
    }

    public UnauthorizedException(Throwable cause) {
        super(cause);
    }
}
