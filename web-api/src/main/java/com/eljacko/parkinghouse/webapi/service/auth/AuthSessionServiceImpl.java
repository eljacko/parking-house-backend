package com.eljacko.parkinghouse.webapi.service.auth;

import com.eljacko.parkinghouse.datamodel.dto.AuthTokenDto;
import com.eljacko.parkinghouse.datamodel.dto.UserLoginDto;
import com.eljacko.parkinghouse.datamodel.entity.user.User;
import com.eljacko.parkinghouse.datamodel.repository.UserRepository;
import com.eljacko.parkinghouse.webapi.dto.auth.AuthData;
import com.eljacko.parkinghouse.webapi.exception.ApplicationException;
import com.eljacko.parkinghouse.webapi.exception.AuthorizationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
@SuppressWarnings("checkstyle:designforextension")
public class AuthSessionServiceImpl implements AuthSessionService {
    private final UserRepository userRepository;

    private final AuthService authService;
    private final AuthTokenService authTokenService;
    private final PasswordService passwordService;

    @Override
    public final AuthData authenticate(final String username, final String password) {
        Assert.notNull(username, "Username must not be null");
        Assert.notNull(password, "Password must not be null");
        AuthData authData = null;
        // try to find user form DB
        if (authData == null) {
            AuthTokenDto authTokenDto = authService.authenticate(username, password);
            if (authTokenDto != null) {
                authData = new AuthData(authTokenDto.getToken(), authTokenDto.getUserId());
            }
        }
        if (authData == null) {
            throw new AuthorizationException();
        }
        return authData;
    }

    @Override
    @Transactional(readOnly = true)
    public UserLoginDto getAuthUserWithoutPassword(@NotNull final String username) {
        Assert.notNull(username, "Username must not be null");

        // get all user with the specified user name
        List<UserLoginDto> loginList = userRepository.getUserLogin(username);
        if (!CollectionUtils.isEmpty(loginList)) {
            List<UserLoginDto> usersWithSessionLogin = loginList.stream()
                    .filter(l -> username.equalsIgnoreCase(l.getEmail()))
                    .collect(Collectors.toList());
            // if there is only one user, then set password
            if (!CollectionUtils.isEmpty(usersWithSessionLogin)) {
                if (usersWithSessionLogin.size() > 1) {
                    log.error("There are more than one user for {}", username);
                    throw new AuthorizationException();
                }
                // verify that password is not set
                UserLoginDto userLogin = usersWithSessionLogin.get(0);
                if (userLogin.getPassword() == null) {
                    return userLogin;
                } else {
                    log.debug("User (username={}, id={}) already has password", username,
                            userLogin.getUserId());
                }
            } else {
                log.debug("There is no user with email", username);
            }
        }
        throw new AuthorizationException();
    }

    @Override
    @Transactional
    public AuthData saveUserPassword(@NotNull final Long userId,
            @NotNull final String plainPassword) {
        Assert.notNull(userId, "User id must not be null");
        Assert.notNull(plainPassword, "Password must not be null");

        User user = userRepository.findById(userId).orElseThrow(() -> {
            log.info("User with id={} not found", userId);
            return new ApplicationException("User not found");
        });
        passwordService.setUserPassword(user, plainPassword);

        // get user token
        return authTokenService.provideToken(userId)
                .map(a -> new AuthData(a.getToken(), a.getUserId()))
                .orElseThrow(() -> new AuthorizationException());
    }

}
