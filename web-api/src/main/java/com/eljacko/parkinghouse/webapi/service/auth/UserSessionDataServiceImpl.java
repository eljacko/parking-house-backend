package com.eljacko.parkinghouse.webapi.service.auth;

import com.eljacko.parkinghouse.datamodel.entity.user.User;
import com.eljacko.parkinghouse.datamodel.repository.UserRepository;
import com.eljacko.parkinghouse.webapi.dto.auth.AuthData;
import com.eljacko.parkinghouse.webapi.session.CurrentSessionUserInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Service
@RequiredArgsConstructor
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class UserSessionDataServiceImpl implements UserSessionDataService {
    private final UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public CurrentSessionUserInfo getUserInfo(final AuthData authData) {
        Assert.notNull(authData, "Auth data must not be null");
        return getUserInfoForUser(authData.getUserId());
    }

    @Override
    @Transactional(readOnly = true)
    public CurrentSessionUserInfo getUserInfo(final CurrentSessionUserInfo currentInfo) {
        CurrentSessionUserInfo info = null;
        if (currentInfo != null && currentInfo.getUser() != null
                && currentInfo.getUser().getId() != null) {
            info = getUserInfoForUser(currentInfo.getUser().getId());
        }
        return info;
    }

    private CurrentSessionUserInfo getUserInfoForUser(final Long userId) {
        Assert.notNull(userId, "User id must not be null");
        CurrentSessionUserInfo info = new CurrentSessionUserInfo();
        User user = userRepository.findById(userId).get();
        info.addUserData(user);
        return info;
    }
}
