package com.eljacko.parkinghouse.webapi.service.user;

import com.eljacko.parkinghouse.datamodel.entity.user.Customer;
import com.eljacko.parkinghouse.datamodel.entity.user.User;
import com.eljacko.parkinghouse.datamodel.entity.user.Vendor;
import com.eljacko.parkinghouse.datamodel.repository.UserRepository;
import com.eljacko.parkinghouse.webapi.dto.user.UserData;
import com.eljacko.parkinghouse.webapi.dto.user.UserDataSaveResult;
import com.eljacko.parkinghouse.webapi.exception.NonUniqueException;
import com.eljacko.parkinghouse.webapi.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Service
@Slf4j
@RequiredArgsConstructor
@SuppressWarnings("checkstyle:designforextension")
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserData getUserData(final Long userId) {
        Assert.notNull(userId, "User id must not be null");
        User userDb = userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException());
        return new UserData(userDb);
    }

    @Override
    @Transactional
    public UserDataSaveResult save(final UserData userData) {
        Assert.notNull(userData, "User data must not be null");
        log.debug("Save user data id={}", userData.getId());
        User userDb;
        boolean isNew = false;
        if (userData.getId() == null) {
            isNew = true;
            if (userData.isVendor()) {
                userDb = new Vendor();
            } else {
                userDb = new Customer();
            }
        } else {
            userDb = userRepository.findById(userData.getId())
                    .orElseThrow(() -> new NotFoundException());
        }

        // verify if name is unique
        if ((isNew || !userDb.getName().equals(userData.getName())) && userRepository
                .existsOtherWithSameName(userData.getName(), userData.getId())) {
            log.debug("There is another user with the same name:{}", userData.getName());
            throw new NonUniqueException("name", "There is another user with the same name");
        }

        userDb.setName(userData.getName());
        userDb.setEmail(userData.getEmail());
        userDb = userRepository.save(userDb);

        UserDataSaveResult result = new UserDataSaveResult();
        result.setUserData(new UserData(userDb));
        return result;
    }
}
