package com.eljacko.parkinghouse.webapi;

import io.undertow.util.Headers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

@Component
@Slf4j
@Order(Integer.MIN_VALUE)
@SuppressWarnings("checkstyle:designforextension")
public class ContentDispositionHeaderSettingFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(final HttpServletRequest request,
                                    final HttpServletResponse response,
                                    final FilterChain chain) throws ServletException, IOException {
        HttpServletResponse wrappedResponse = new HttpServletResponseWrapper(response) {
            @Override
            public ServletOutputStream getOutputStream() throws IOException {
                final ServletResponse response = this.getResponse();
                if (response != null && "application/json"
                        .equalsIgnoreCase(response.getContentType())) {
                    this.setHeader(Headers.CONTENT_DISPOSITION_STRING,
                            "attachment; filename=api.json");
                }
                return super.getOutputStream();
            }
        };
        chain.doFilter(request, wrappedResponse);
    }
}
