package com.eljacko.parkinghouse.webapi.service.parking;

import com.eljacko.parkinghouse.webapi.dto.parking.ParkingCommand;
import com.eljacko.parkinghouse.webapi.dto.parking.ParkingData;
import com.eljacko.parkinghouse.webapi.dto.parking.ParkingDataSaveResult;

public interface ParkingService {

    ParkingDataSaveResult save(ParkingData parkingData);

    ParkingDataSaveResult processCommand(ParkingCommand parkingData);

    ParkingData getParkingData(Long parkingId);

}
