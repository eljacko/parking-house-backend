package com.eljacko.parkinghouse.webapi.controller.common;

import com.eljacko.parkinghouse.webapi.constant.ErrorCodes;
import com.eljacko.parkinghouse.webapi.dto.response.CommonErrorResponse;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class AbstractCommonController {

    protected final PageRequest getPageRequest(final Integer offset, final Integer limit) {
        PageRequest page = null;
        Optional<Integer> offsetOptional = Optional.ofNullable(offset);
        Optional<Integer> limitOptional = Optional.ofNullable(limit);
        if (offsetOptional.filter(o -> o > 0).isPresent()
                || limitOptional.filter(o -> o > 0).isPresent()) {
            page = PageRequest.of(offsetOptional.orElse(0), limitOptional.orElse(0));
        }
        return page;
    }

    protected final Sort getSort(final String orderBy, final boolean isDesc) {
        Sort sort = null;
        if (!StringUtils.isEmpty(orderBy)) {

            Sort.Direction orderDirection = Sort.Direction.DESC;
            if (!isDesc) {
                orderDirection = Sort.Direction.ASC;
            }
            sort = Sort
                    .by(new Sort.Order(orderDirection, orderBy));
        }
        return sort;
    }

    protected final Sort getSort(final String sortingData, final Set<String> allowedValues) {
        Sort sort = null;
        if (!StringUtils.isEmpty(sortingData)) {
            List<Sort.Order> sortOrders = Arrays
                    .asList(sortingData.split(",")).stream().map(String::trim)
                    .map(str -> str.split(":")).map(arr -> getSortObj(arr, allowedValues))
                    .collect(Collectors.toList());

            sort = Sort.by(sortOrders);
        }
        return sort;
    }

    private Sort.Order getSortObj(final String[] arr, final Set<String> allowedValues) {
        Sort.Order sort = null;
        if (arr.length > 0) {
            String orderBy = arr[0].trim().toLowerCase();

            if (!allowedValues.contains(orderBy)) {
                throw new IllegalArgumentException("Unknown order property");
            }
            Sort.Direction orderDirection = Sort.Direction.ASC;
            if (arr.length == 2) {
                String orderDirectionStr = arr[1].trim().toLowerCase();
                orderDirection = Sort.Direction.fromString(orderDirectionStr);
            }
            sort = new Sort.Order(orderDirection, orderBy);
        }
        return sort;
    }

    protected final CommonErrorResponse getGoneErrorResponse(final String newResourcePath) {
        CommonErrorResponse error = new CommonErrorResponse();
        error.setTimestampAsStr(Instant.now());
        error.setMessage("This resource is gone. Please use: " + newResourcePath);
        error.setCode(ErrorCodes.APPLICATION_EXCEPTION);
        return error;
    }

}
