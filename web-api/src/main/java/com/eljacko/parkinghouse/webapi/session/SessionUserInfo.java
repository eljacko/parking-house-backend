package com.eljacko.parkinghouse.webapi.session;

public interface SessionUserInfo {
    SessionUser getUser();

}
