package com.eljacko.parkinghouse.webapi.service.util;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;

public interface DateUtilService {

    java.util.Date getDate();

    Timestamp getCurrentTimeAsTimestamp();

    Instant getInstantNow();

    Timestamp getPastTimestamp(Integer minutes);

    Timestamp getTimeAsTimestamp(LocalDateTime time);

}
