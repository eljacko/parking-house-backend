package com.eljacko.parkinghouse.webapi.session;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class SessionVendor implements Serializable {
    private static final long serialVersionUID = 3794718737789062220L;
    private Long id;
    private String name;
}
