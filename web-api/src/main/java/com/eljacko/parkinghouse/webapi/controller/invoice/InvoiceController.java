package com.eljacko.parkinghouse.webapi.controller.invoice;

import com.eljacko.parkinghouse.datamodel.dto.CustomerPackage;
import com.eljacko.parkinghouse.webapi.constant.WebApiPaths;
import com.eljacko.parkinghouse.webapi.controller.AbstractController;
import com.eljacko.parkinghouse.webapi.dto.invoice.InvoiceData;
import com.eljacko.parkinghouse.webapi.service.auth.SessionUserPermissionValidator;
import com.eljacko.parkinghouse.webapi.service.invoice.InvoiceService;
import com.eljacko.parkinghouse.webapi.service.util.DateUtilService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Slf4j
@Getter
@RequiredArgsConstructor
@RequestMapping("/stateful/v1")
@SuppressWarnings("checkstyle:designforextension")
public class InvoiceController extends AbstractController {
    private final SessionUserPermissionValidator sessionParkingPermissionValidator;
    private final InvoiceService invoiceService;
    private final DateUtilService dateUtilService;

    @PostMapping(WebApiPaths.INVOICE)
    public HttpEntity<?> getGenerate(@RequestBody @Valid final InvoiceData invoiceData) {
        log.debug("Request for invoice with data {}", invoiceData);
        //sessionParkingPermissionValidator.validateVendorPermission(parkingData.getVendorId());

        CustomerPackage result = invoiceService.getInvoiceForUser(invoiceData);
        log.debug("Invoice data result: invoiceId:{}", result.getId());
        return new ResponseEntity<CustomerPackage>(result, HttpStatus.OK);
    }
}
