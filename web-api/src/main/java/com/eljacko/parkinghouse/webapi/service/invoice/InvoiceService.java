package com.eljacko.parkinghouse.webapi.service.invoice;

import com.eljacko.parkinghouse.datamodel.dto.CustomerPackage;
import com.eljacko.parkinghouse.webapi.dto.invoice.InvoiceData;

public interface InvoiceService {

    CustomerPackage getInvoiceForUser(InvoiceData invoiceData);

}
