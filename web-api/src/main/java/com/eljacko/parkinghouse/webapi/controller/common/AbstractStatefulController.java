package com.eljacko.parkinghouse.webapi.controller.common;

import com.eljacko.parkinghouse.webapi.exception.UnauthorizedException;
import com.eljacko.parkinghouse.webapi.session.CurrentSessionData;
import org.springframework.util.StringUtils;

public abstract class AbstractStatefulController extends AbstractCommonController {

    public final Long getUserId() {
        if (getSessionData() != null
                && getSessionData().getUserInfo() != null
                && getSessionData().getUserInfo().getUser() != null) {
            return getSessionData().getUserInfo().getUser().getId();
        }
        return null;
    }
    protected abstract CurrentSessionData getSessionData();

    protected final String getSessionToken() {
        if (getSessionData() != null && !StringUtils.isEmpty(getSessionData().getToken())) {
            return getSessionData().getToken();
        }
        throw new UnauthorizedException();
    }

}
