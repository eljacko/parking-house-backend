package com.eljacko.parkinghouse.webapi.exception;

@SuppressWarnings({ "checkstyle:FinalParameters" })
public class WrongTokenException extends RuntimeException {
    private static final long serialVersionUID = 4076119488371982779L;

    public WrongTokenException() {
        super();
    }

    public WrongTokenException(final String message) {
        super(message);
    }

    public WrongTokenException(final String message, Throwable cause) {
        super(message, cause);
    }

    public WrongTokenException(Throwable cause) {
        super(cause);
    }
}
