package com.eljacko.parkinghouse.webapi.session;

import com.eljacko.parkinghouse.datamodel.utils.CurrentAuditor;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@ToString
@EqualsAndHashCode
public class CurrentSessionUser implements SessionUser, Serializable {
    private static final long serialVersionUID = 9132503246693875616L;
    private Long id;
    private String name;
    @Setter
    private String email;
    @JsonIgnore
    @Setter(AccessLevel.NONE)
    private CurrentAuditor auditor;

    public CurrentSessionUser() {
        super();
        auditor = new CurrentAuditor("user");
    }

    public final void setId(final Long id) {
        this.id = id;
        auditor.setId(id);
    }

    public final void setName(final String name) {
        this.name = name;
        auditor.setName(name);
    }

    @JsonIgnore
    public final boolean isLoggedIn() {
        if (getId() != null) {
            return true;
        }
        return false;
    }

}
