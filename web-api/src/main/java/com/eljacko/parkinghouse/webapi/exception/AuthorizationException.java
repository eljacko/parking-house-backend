package com.eljacko.parkinghouse.webapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings({ "checkstyle:FinalParameters" })
@ResponseStatus(HttpStatus.FORBIDDEN)
public class AuthorizationException extends RuntimeException {
    private static final long serialVersionUID = 4076119488371982779L;

    public AuthorizationException() {
        super();
    }

    public AuthorizationException(final String message) {
        super(message);
    }

    public AuthorizationException(final String message, Throwable cause) {
        super(message, cause);
    }

    public AuthorizationException(Throwable cause) {
        super(cause);
    }
}

