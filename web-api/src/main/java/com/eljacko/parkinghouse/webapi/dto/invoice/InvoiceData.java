package com.eljacko.parkinghouse.webapi.dto.invoice;

import com.eljacko.parkinghouse.datamodel.constant.ValidationMessages;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class InvoiceData {
    @NotNull(message = ValidationMessages.NOT_NULL)
    private Long vendorId;
    @NotNull(message = ValidationMessages.NOT_NULL)
    private Long customerId;
    @NotNull(message = ValidationMessages.NOT_NULL)
    @NotBlank
    private String billingYear;
    @NotNull(message = ValidationMessages.NOT_NULL)
    @NotBlank
    private String billingMonth;

    public InvoiceData() {
        super();
    }
}
