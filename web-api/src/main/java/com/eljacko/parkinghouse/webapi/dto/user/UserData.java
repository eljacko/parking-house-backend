package com.eljacko.parkinghouse.webapi.dto.user;

import com.eljacko.parkinghouse.datamodel.constant.UserType;
import com.eljacko.parkinghouse.datamodel.constant.ValidationMessages;
import com.eljacko.parkinghouse.datamodel.entity.user.User;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class UserData {
    private Long id;
    @NotNull(message = ValidationMessages.NOT_NULL)
    @NotBlank
    private String name;
    private String email;
    private boolean vendor;

    public UserData(final User user) {
        super();
        setId(user.getId());
        setName(user.getName());
        setEmail(user.getEmail());
        setVendor(user.getType().getId().equals(UserType.VENDOR.getId()));
    }
}
