package com.eljacko.parkinghouse.webapi.service.invoice;

import com.eljacko.parkinghouse.datamodel.dto.CustomerPackage;
import com.eljacko.parkinghouse.datamodel.entity.Parking;
import com.eljacko.parkinghouse.datamodel.entity.invoice.Invoice;
import com.eljacko.parkinghouse.datamodel.entity.invoice.InvoiceParking;
import com.eljacko.parkinghouse.datamodel.entity.product.MonthlyPackage;
import com.eljacko.parkinghouse.datamodel.entity.user.Customer;
import com.eljacko.parkinghouse.datamodel.entity.user.Vendor;
import com.eljacko.parkinghouse.datamodel.repository.CustomerRepository;
import com.eljacko.parkinghouse.datamodel.repository.invoice.InvoiceParkingRepository;
import com.eljacko.parkinghouse.datamodel.repository.ParkingRepository;
import com.eljacko.parkinghouse.datamodel.repository.VendorRepository;
import com.eljacko.parkinghouse.datamodel.repository.InvoiceRepository;
import com.eljacko.parkinghouse.datamodel.utils.DateUtils;
import com.eljacko.parkinghouse.webapi.dto.invoice.InvoiceData;
import com.eljacko.parkinghouse.webapi.exception.InvalidParameterException;
import com.eljacko.parkinghouse.webapi.exception.NotFoundException;
import com.eljacko.parkinghouse.webapi.service.monthlypackage.PackageService;
import com.eljacko.parkinghouse.webapi.service.util.DateUtilService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Log4j2
public class InvoiceServiceImpl implements InvoiceService {
    private final InvoiceRepository invoiceRepository;
    private final CustomerRepository customerRepository;
    private final VendorRepository vendorRepository;
    private final ParkingRepository parkingRepository;
    private final InvoiceParkingRepository invoiceParkingRepository;
    private final PackageService packageService;

    private final DateUtilService dateUtilService;


    @Override
    public CustomerPackage getInvoiceForUser(InvoiceData invoiceData) {
        Assert.notNull(invoiceData, "Invoice data must not be null");
        Vendor vendorDb = vendorRepository.findById(invoiceData.getVendorId()).orElseThrow(() -> {
            log.debug("There is no vendor with id={}", invoiceData.getVendorId());
            return new NotFoundException("There is no vendor with such id");
        });
        Customer customerDb = customerRepository.findById(invoiceData.getCustomerId())
                .orElseThrow(() -> {
                    log.debug("There is no customer with id={}", invoiceData.getCustomerId());
                    return new NotFoundException("There is no customer with such id");
                });

        Optional invoiceOpt = invoiceRepository
                .findByBillingMonthAndBillingYearAndCustomerAndVendor(
                        invoiceData.getBillingMonth(),
                        invoiceData.getBillingYear(),
                        customerDb,
                        vendorDb);
        if (invoiceOpt.isPresent()) {
            return new CustomerPackage((Invoice) invoiceOpt.get());
        }

        Invoice invoiceDb = new Invoice();
        invoiceDb.setDateIssued(dateUtilService.getCurrentTimeAsTimestamp());
        invoiceDb.setCustomer(customerDb);
        invoiceDb.setVendor(vendorDb);
        invoiceDb.setBillingMonth(invoiceData.getBillingMonth());
        invoiceDb.setBillingYear(invoiceData.getBillingYear());
        MonthlyPackage monthlyPackageDb = packageService
                .getActivePackageWithVendor(customerDb, vendorDb);
        invoiceDb.setMonthlyPackage(monthlyPackageDb);

        Date fromDate;
        Date tillDate;
        List<Parking> parkingsListDb;
        try {
            fromDate = new SimpleDateFormat("MMyyyy")
                    .parse(invoiceData.getBillingMonth() + invoiceData.getBillingYear());
            tillDate = DateUtils.addMonth(fromDate);
            parkingsListDb = parkingRepository.findByEndTimeBetweenAndCustomerAndVendor(
                    fromDate, tillDate, customerDb, vendorDb);
        } catch (ParseException e) {
            log.error("Exception", e);
            throw new InvalidParameterException("billingMonthYear", "Wrong format");
        }

        Set<InvoiceParking> parkingsDb = new HashSet<>();
        Integer totalParkingFee = 0;
        if (!parkingsListDb.isEmpty()) {
            for (Parking parking : parkingsListDb) {
                InvoiceParking ip = new InvoiceParking(parking, invoiceDb);
                Integer parkingFee = calculateParkingFee(monthlyPackageDb, parking);
                ip.setParkingFee(parkingFee);
                totalParkingFee = totalParkingFee + parkingFee;
                invoiceParkingRepository.save(ip);
                parkingsDb.add(ip);
            }
        }

        Integer totalFee = monthlyPackageDb.getPrice() + totalParkingFee;
        Integer maxInvoice = monthlyPackageDb.getProductInfo().getMaximumInvoice();
        if (maxInvoice != null && totalFee > maxInvoice) {
            totalFee = maxInvoice;
        }
        invoiceDb.setTotalAmount(totalFee);
        invoiceDb.setParkings(parkingsDb);
        invoiceRepository.save(invoiceDb);
        return new CustomerPackage(invoiceDb);
    }

    private Integer calculateParkingFee(MonthlyPackage monthlyPackage, Parking parking) {
        Duration d = Duration.between(parking.getStartTime().toInstant(),
                parking.getEndTime().toInstant());
        Double fee = monthlyPackage.getProductInfo().getParkingFee().getPrice()
                * Math.ceil(d.toMinutes() / 30);
        return fee.intValue();
    }

}
