package com.eljacko.parkinghouse.webapi.service.user;

import com.eljacko.parkinghouse.webapi.dto.user.UserData;
import com.eljacko.parkinghouse.webapi.dto.user.UserDataSaveResult;

public interface UserService {

    UserDataSaveResult save(UserData userData);

    UserData getUserData(Long userId);

}
