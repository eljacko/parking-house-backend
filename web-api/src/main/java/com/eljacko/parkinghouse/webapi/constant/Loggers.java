package com.eljacko.parkinghouse.webapi.constant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Loggers {
    public static final Logger ACCESS_LOG_LOGGER = LoggerFactory
            .getLogger("access_log");

    private Loggers() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
