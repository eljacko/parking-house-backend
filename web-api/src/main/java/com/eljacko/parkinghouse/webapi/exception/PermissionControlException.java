package com.eljacko.parkinghouse.webapi.exception;

@SuppressWarnings({ "checkstyle:FinalParameters" })
public class PermissionControlException extends RuntimeException {
    private static final long serialVersionUID = 4076119488371982779L;

    public PermissionControlException() {
        super();
    }

    public PermissionControlException(final String message) {
        super(message);
    }

    public PermissionControlException(final String message, Throwable cause) {
        super(message, cause);
    }

    public PermissionControlException(Throwable cause) {
        super(cause);
    }
}
