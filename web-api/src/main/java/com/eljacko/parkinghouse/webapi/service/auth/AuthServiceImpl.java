package com.eljacko.parkinghouse.webapi.service.auth;

import com.eljacko.parkinghouse.datamodel.dto.AuthTokenDto;
import com.eljacko.parkinghouse.datamodel.dto.UserLoginDto;
import com.eljacko.parkinghouse.datamodel.repository.UserRepository;
import com.eljacko.parkinghouse.webapi.exception.AuthorizationException;
import com.eljacko.parkinghouse.webapi.service.util.BCrypt;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
@SuppressWarnings("checkstyle:designforextension")
public class AuthServiceImpl implements AuthService {
    private final UserRepository userRepository;

    private final AuthTokenService authTokenService;

    @Override
    @Transactional
    public AuthTokenDto authenticate(final String username, final String password,
                                           final String apiKey) {
        Assert.notNull(apiKey, "API key must not be null");
        return authenticateCommon(username, password, apiKey);
    }

    @Override
    @Transactional
    public AuthTokenDto authenticate(final String username, final String password) {
        return authenticateCommon(username, password, null);
    }

    private AuthTokenDto authenticateCommon(final String username, final String password,
            final String apiKey) {
        Assert.notNull(username, "Username must not be null");
        Assert.notNull(password, "Password must not be null");
        AuthTokenDto tokenDto = null;
        log.debug("start authenticate for {}", username);

        List<UserLoginDto> loginList = userRepository.getUserLogin(username);
        if (!CollectionUtils.isEmpty(loginList)) {
            UserLoginDto loginDto = null;
            // verify password and PIN
            boolean matched = false;
            for (UserLoginDto loginData : loginList) {
                if (!matched && !StringUtils.isEmpty(loginData.getPassword())
                        && username.equalsIgnoreCase(loginData.getEmail())) {
                    matched = BCrypt.checkpw(password, loginData.getPassword());
                }
                if (matched) {
                    if (loginDto == null) {
                        loginDto = loginData;
                    } else {
                        log.error("There are more than one user for {}: {}, {} ", username,
                                loginDto.getUserId(), loginData.getUserId());
                        throw new AuthorizationException();
                    }
                }
            }
            if (loginDto != null) {
                if (apiKey == null) {
                    // get token or create new one if there is no
                    Optional<AuthTokenDto> tokenDtoOpt = authTokenService
                            .provideToken(loginDto.getUserId());
                    if (tokenDtoOpt.isPresent()) {
                        tokenDto = tokenDtoOpt.get();
                    }
                } else {
                    throw new AuthorizationException();
                }
            }
        }
        if (tokenDto != null) {
            return tokenDto;
        } else {
            throw new AuthorizationException();
        }

    }
}
