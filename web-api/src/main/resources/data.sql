INSERT INTO public.user_type ("id", "name") VALUES (1, 'VENDOR') ON CONFLICT DO NOTHING;
INSERT INTO public.user_type ("id", "name") VALUES (2, 'CUSTOMER') ON CONFLICT DO NOTHING;
INSERT INTO public.product_type ("id", "name") VALUES (1, 'MONTHLY_FEE') ON CONFLICT DO NOTHING;
INSERT INTO public.product_type ("id", "name") VALUES (2, 'PARKING_FEE') ON CONFLICT DO NOTHING;

